// MetaProgrammingTypeList.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include "../include/Binary.hpp"
#include "../include/TypeList.hpp"
#include "../include/IsSame.hpp"
#include "../include/Contains.hpp"
#include "../include/AtIndex.hpp"
#include "../include/PrintIt.hpp"
#include "../include/Remove.hpp"
#include "../include/Tuple.hpp"

#include "../../googletest/googletest/src/gtest-all.cc"

// These two are the old-style typedefs. But macros can do, so that it's not fixed to int's.
typedef TypeList<int,NullType> INTTL;
typedef TypeList<int, TypeList<int, TypeList<int, TypeList<int, NullType>>>> tl;


// Exercise 3.2.2 struct Contains<>
typedef TYPELIST3( int, char, long ) testTL;

// Exercise 3.2.3
typedef TYPELIST3( long, char, int ) test1TL;



TEST( BINARY, Works )
{
    Binary< 11111111 > binaryCheckz;
    int val = binaryCheckz.value;
    EXPECT_EQ( val, 255 );  

    Binary< 101 > binaryOther;
    val = binaryOther.value;
    EXPECT_EQ( val, 5 );
}

TEST( TYPELIST, CountedVals )
{
    typedef TypeList< int, TypeList< char, TypeList< double, NullType > > > someTypeList;
    int val = Count< someTypeList >::value;
    EXPECT_EQ( val, 3);

    typedef TYPELIST4( int, long, double, char ) someOtherType;
    val = Count< someOtherType >::value;
    EXPECT_EQ( val, 4 );
}


TEST( ISSAME, IfWorks )
{
    bool val = IsSame< int, int >::value;
    EXPECT_EQ( val, true );

    val = IsSame< char, int >::value;
    EXPECT_NE( val, true );
}

TEST( CONTAINS, IfWorks )
{
    /* Must be true! */
    bool trueOrNot = Contains< testTL, int >::value;
    EXPECT_EQ( trueOrNot, true );
    /* Must be false! */
    trueOrNot = Contains< testTL, std::string >::value;
    EXPECT_NE( trueOrNot, true );
}

TEST( ATINDEX, IfWorks )
{
    bool trueOrNot = IsSame< typename AtIndex<test1TL, 2>::type, int>::value;
    EXPECT_EQ( trueOrNot, true );
    
    trueOrNot = IsSame<typename AtIndex<test1TL, 2>::type, char>::value;
    EXPECT_NE( trueOrNot, true );
}

TEST( REMOVE, IfWorks )
{
    // Use test1TL
    //bool trueOrNot = Contains<typename Remove<test1TL, int>::type, int>::value;

    //trueOrNot = Contains<typename Remove<test1TL, char>::type, int>::value;

    //bool falseOrNot = Remove< test1TL, int >::type;
}


int main( int argc, char** args )
{	
    testing::InitGoogleTest( &argc, args );
    return RUN_ALL_TESTS();
}