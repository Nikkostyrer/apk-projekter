#ifndef __REMOVE_HPP__
#define __REMOVE_HPP__
#include "TypeList.hpp"
#include "IsSame.hpp"


// // The general one
// // The one invoked from main
// template< typename U, typename V, typename = void > // <-- Allows user to invoke with 2 params.
// struct Remove
// {
//     using type = typename Remove< typename U
// };

// template< typename T, typename U > // If they are not the same
// struct Remove< T, U, std::void_t<( IsSame< typename ( std::declval< T >()::First ), U > )>> 
// {
//     using type = typename Remove< typename T::Rest::Rest, U >::type;
// };





#endif //__REMOVE_HPP__