// General Contains
// Defines that it needs to be created with 3 params
#ifndef __CONTAINS_HPP__
#define __CONTAINS_HPP__

#include "TypeList.hpp"


// The general one, invoked from main.
template< typename T, typename Q >
struct Contains
{
    static const bool value = ( IsSame< typename T::First, Q >::value ) ? true : Contains< typename T::Rest, Q >::value;
};

// The specialized one with the nulltype
template <typename Q>
struct Contains< NullType, Q>
{
    static const bool value = false;
};




#endif //__CONTAINS