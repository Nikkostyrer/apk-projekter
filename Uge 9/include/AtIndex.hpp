#ifndef __ATINDEX_HPP__
#define __ATINDEX_HPP__

#include <stdlib.h>

template< typename T, size_t N >
struct AtIndex
{
    using type = typename AtIndex< typename T::Rest, ( N-1 ) >::type;
};

template< typename T >
struct AtIndex< T, 0 >
{
    using type = typename T::First; 
};






#endif //__ATINDEX_HPP__