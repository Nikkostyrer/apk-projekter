#ifndef __TYPELIST_HPP__
#define __TYPELIST_HPP__

// Here lies the implementation for exercise 3.

// Definition of macros for listing 3.2 in ex. 3.1
#define TYPELIST1( arg1 ) TypeList< arg1, NullType >
#define TYPELIST2( arg1, arg2 ) TypeList< arg1, TYPELIST1( arg2 ) >
#define TYPELIST3( arg1, arg2, arg3 ) TypeList< arg1, TYPELIST2( arg2, arg3 ) >
#define TYPELIST4( arg1, arg2, arg3, arg4 ) TypeList< arg1, TYPELIST3( arg2, arg3, arg4 ) >

// Definition of the nulltype, that acts as a stopping class from recursion.
struct NullType{};

// A typelist has a first-value, that can be anything.
// The "Rest" HAS to be either a nulltype, or a TypeList.
template <typename L, typename R>
struct TypeList {
	typedef L First;
	typedef R Rest;
};

// A struct Count, that creates a new Count with the value of "Rest"
// Remember why Rest was either nulltype or TypeList??
// It creates a Count struct with the template type of "Rest"
    // Now imagine if "Rest" was actually an int! Does an int have a member "Rest"??

// And it has to have a nulltype at some point,
    // 'Cause it won't stop until it reaches the nulltype.
template< typename TL >
struct Count
{
	static const int value = 1 + Count<typename TL::Rest>::value;
};

// This works as a stopper.
template<>
struct Count< NullType >
{
	static const int value = 0;
};


#endif //__TYPELIST_HPP__