// Here lies the answer to exercise 2.

#include <stdlib.h>
// Construction of a class isBinary, which per default has no value.
template< size_t N >
struct isBinary {};

// But the specialization states that it has a value when it's constructed with 0.
template<>
struct isBinary< 0 > 
{
	static const size_t value = 0;
};

// The same applies to the one created with a 1.
template<>
struct isBinary< 1 > {
	static const size_t value = 1;
};


// Construction of the Binary class that is to be called.
// This recursively calls Binary.
// Also calls isBinary, which checks if it actually is 1 or 0 that is left from the %.
template <size_t N>
struct Binary {

	static const size_t value = Binary<N / 10>::value << 1 | isBinary<N % 10>::value;
};

// Stopping point, to stop the recursiveness.
template<>
struct Binary<0>
{
	static const size_t value = 0;
};