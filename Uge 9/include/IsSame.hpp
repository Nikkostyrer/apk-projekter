#ifndef __ISSAME_HPP__
#define __ISSAME_HPP__

template< typename T, typename U >
struct IsSame
{
    static const bool value = false;
};

template< typename T >
struct IsSame< T, T > 
{
    static const bool value = true;
};


#endif //__ISSAME_HPP__