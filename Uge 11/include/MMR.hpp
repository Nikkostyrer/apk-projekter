/*****************************************/
/* Copyright: DevelEdu 2015              */
/* Author: sha                           */
/*****************************************/
#ifndef MMR_HPP_
#define MMR_HPP_

#include <stdlib.h>
#include <cstdint>

template<size_t N>
struct Binary
{
  enum{ value = Binary<N/10>::value << 1 | N%10 };
};

template<>
struct Binary < 0 >
{
  enum  { value = 0 };
};


// 1.2.1 Reading policy
struct RO
{
	static uint32_t read( uint32_t* address, uint32_t offsetBegin, uint32_t offsetEnd )
	{
		// Retrieve the value in the address
		uint32_t valueOfAddress = ( *reinterpret_cast< volatile uint32_t* >( address ) );

		// Create bitmask.
		uint32_t bitMask = ( static_cast< uint32_t >( 1 ) << ( offsetEnd - offsetBegin + 1 ) ) - 1;
	
		// Shifting values down to same offset as the mask.
		valueOfAddress = valueOfAddress >> offsetBegin;

		// Applying mask.
		valueOfAddress &= bitMask;

		// Return that:
		return valueOfAddress;
	}
};



// 1.2.2 Writing policy
struct WO
{
	static void write( uint32_t* address, uint32_t offsetBegin, uint32_t offsetEnd, uint32_t value )
	{
		// Create a pointer to the address we want changed.
		volatile uint32_t* toChange = reinterpret_cast< volatile uint32_t* >( address );
		std::cout << "The value of the thing to change: " << std::bitset<32>(*toChange) << "\n";
		// Create bistmask.
		uint32_t bitMask = value;//( static_cast< uint32_t >( 1 ) << ( offsetEnd - offsetBegin + 1 ) ) - 1;
		std::cout << "The value of the bitmask: " << std::bitset<32>(bitMask) << "\n";
		// Use bitmask with value.
		value &= bitMask;
		std::cout << "The value after &=: " << std::bitset<32>( value ) << "\n";
		// Shift the value up with the offset.
		value = value << offsetBegin;
		std::cout << "The value after shifting mask up: " << std::bitset< 32 >( value ) << "\n";
		// Apply values to address:
		( *toChange ) |= value;
		( *toChange ) = ~(*toChange);
		std::cout << "The changed thin after |=: " << std::bitset<32>(*toChange) << "\n";
		( *toChange ) &= ( ~value );
		std::cout << "The changed thing after &= (~): " << std::bitset<32>(*toChange)<<"\n";
		( *toChange ) = ~( *toChange );
	}
};


// 1.2.3 Reading / writing policy
struct RW : public RO
{
	static void write( uint32_t* address, uint32_t offsetBegin, uint32_t offsetEnd, uint32_t value )
	{
		uint32_t whatsReadAndModified = read( address, offsetBegin, offsetEnd);

		WO::write( &whatsReadAndModified, offsetBegin, offsetEnd, value );

		( *reinterpret_cast< volatile uint32_t* >( address ) ) = whatsReadAndModified;
	}
};


// 1.3.1 Default Translator - already done for you
struct DefaultTranslator
{
  uint32_t out(uint32_t o)
  {
      return o;
  }
  
  typedef uint32_t InRetType;
  uint32_t in(uint32_t i)
  {
	  return i;
  }
};




// 1.4.1 - 1.4.4 BitField
template< typename AccessPolicy, uint32_t offsetBegin, uint32_t offsetEnd,
		  typename Translator = DefaultTranslator >
struct BitField : public Translator
{
	BitField( size_t* address )
	{
		addressLooker = address;
	}

	uint32_t* addressLooker = nullptr;
};




// 1.3.2 State Translator
enum class State
{
	DISABLE = 0,
	ENABLE = 1,

	FALSE = 100
};

template< size_t En, size_t Dis >
struct StateTranslator
{
	typedef State InOutType;

	InOutType in( size_t i )
	{
		if ( i == En )
		{
			return State::ENABLE;
		}
		else if ( i == Dis )
		{
			return State::DISABLE;
		}
		else
		{
			return State::FALSE;
		}
	}

	uint32_t out( State o )
	{
		if ( o == State::ENABLE )
		{
			return En;
		}
		else if ( o == State::DISABLE )
		{
			return Dis;
		}
		else
		{
			return 100;
		}		
	}
};



// 1.3.3 Prescale Translator
enum class Prescale
{
DIV_FACT_2   = Binary<001>::value, 
DIV_FACT_4   = Binary<010>::value,
DIV_FACT_8   = Binary<011>::value,
DIV_FACT_16  = Binary<100>::value,
DIV_FACT_32  = Binary<101>::value,
DIV_FACT_64  = Binary<110>::value,
DIV_FACT_128 = Binary<111>::value,
DIV_ERROR	 = false
};

struct PrescaleTranslator
{
	typedef Prescale InOutType;

	InOutType in( uint32_t i )
	{
		switch ( i )
		{
		case Binary<001>::value:
			return Prescale::DIV_FACT_2;
			break;
		case Binary<010>::value:
			return Prescale::DIV_FACT_4;
			break;
		case Binary<011>::value:
			return Prescale::DIV_FACT_8;
			break;
		case Binary<100>::value:
			return Prescale::DIV_FACT_16;
			break;
		case Binary<101>::value:
			return Prescale::DIV_FACT_32;
			break;
		case Binary<110>::value:
			return Prescale::DIV_FACT_64;
			break;
		case Binary<111>::value:
			return Prescale::DIV_FACT_128;
			break;
		default:
			return Prescale::DIV_ERROR;
			break;
		}
	}

	uint32_t out( InOutType o )
	{
		switch ( o )
		{
		case Prescale::DIV_FACT_2:
			return Binary<001>::value;
			break;
		case Prescale::DIV_FACT_4:
			return Binary<010>::value;
			break;
		case Prescale::DIV_FACT_8:
			return Binary<011>::value;
			break;
		case Prescale::DIV_FACT_16:
			return Binary<100>::value;
			break;
		case Prescale::DIV_FACT_32:
			return Binary<101>::value;
			break;
		case Prescale::DIV_FACT_64:
			return Binary<110>::value;
			break;
		case Prescale::DIV_FACT_128:
			return Binary<111>::value;
			break;
		default:
			return false;
			break;
		}
	}
};

#endif
