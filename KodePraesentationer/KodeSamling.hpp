
// Anonynous namespace below.
/**
 * @brief THIS NAMESPACE is used to show how an anonymous namespace is made.
 * 
 * It contains a functinos that looke horrible. So don't look at it.
 * 
 */
class Item;
namespace
{
    template< typename ItemPtr, typename DeductionFunction >
    Item* generateSingleItem( ItemPtr ptrIn, DeductionFunction funcIn )
    {
        return funcIn( ptrIn );
    }


    template< typename Iterator >
    struct DeduceDrugType
    {
        DeduceDrugType( Iterator& iterIn ) : iter( iterIn ){}

        Item* operator()( int index )
        {
            testPtr = nullptr;

            Item* actualItemPtr = reinterpret_cast< Item* >( iter.at( index ) ) ;

            // Call function that basically just calls the lambda.
            return generateSingleItem( actualItemPtr, 
                                     []
                                     ( Item* itemPtrFromAlgo )
                    {
                        int weed, lsd, mushroom, heroine, cocaine;

                        itemPtrFromAlgo->checkValues( weed, lsd, mushroom, heroine, cocaine );

                        if(weed == 1 && lsd == 0 && mushroom == 0 && heroine == 0 && cocaine == 0)
                        {
                            return reinterpret_cast< Item* >( new dl::Weed( itemPtrFromAlgo->getQuantity() ) );    
                        }     
                        else if(weed == 0 && lsd == 1 && mushroom == 0 && heroine == 0 && cocaine == 0)
                        {
                            return reinterpret_cast< Item* >( new dl::LSD( itemPtrFromAlgo->getQuantity() ) );   
                        }     
                        else if(weed == 0 && lsd == 0 && mushroom == 1 && heroine == 0 && cocaine == 0)
                        {
                            return reinterpret_cast< Item* >( new dl::Mushroom( itemPtrFromAlgo->getQuantity() ) );     
                        }    
                        else if(weed == 0 && lsd == 0 && mushroom == 0 && heroine == 1 && cocaine == 0)
                        {
                            return reinterpret_cast< Item* >( new dl::Heroine( itemPtrFromAlgo->getQuantity() ) );  
                        }     
                        else if(weed == 0 && lsd == 0 && mushroom == 0 && heroine == 0 && cocaine == 1)
                        {
                            return reinterpret_cast< Item* >(new dl::Cocaine( itemPtrFromAlgo->getQuantity() ) );   
                        }     
                        else if(weed == 1 && lsd == 1 && mushroom == 0 && heroine == 0 && cocaine == 0)
                        {
                            return reinterpret_cast< Item* >(new dl::Drug<1,1,0,0,0>( itemPtrFromAlgo->getQuantity() ));
                        }     
                        else if(weed == 0 && lsd == 0 && mushroom == 1 && heroine == 0 && cocaine == 1)
                        {
                            return reinterpret_cast< Item* >(new dl::Drug<0,0,1,0,1>( itemPtrFromAlgo->getQuantity() ));
                        }     
                        else if(weed == 1 && lsd == 0 && mushroom == 0 && heroine == 0 && cocaine == 1)
                        {
                            return reinterpret_cast< Item* >(new dl::Drug<1,0,0,0,1>( itemPtrFromAlgo->getQuantity() ));
                        }  
                    } );
        }

        Iterator iter;

        Item* testPtr;

        // int weed, lsd, mushroom, heroine, cocaine;
    };
    /****************************************************************************************/

    template< typename Iterator, typename OtherIterator, typename DoableFunction >
    void GenerateItems( Iterator begin, Iterator end, OtherIterator to, DoableFunction iffer )
    {
        int count = 0;
        for ( ; begin != end; begin++, count++ )
        {
            ( reinterpret_cast< void*& >( to.at( count ) ) ) = iffer( count );
        }
    }
    /****************************************************************************************/

    template< typename Iterator, typename OtherIterator >
    void copyAlgoSpecificToItems( Iterator begin, Iterator end, OtherIterator to )
    {
        GenerateItems( begin, end, to, DeduceDrugType( begin ) );   
    }
    /****************************************************************************************/
} // Ending anonymous namespace





template< bool, typename True, typename False >
struct IfThenElse
{
    typedef False type;
};

template< typename True, typename False >
struct IfThenElse< true, True, False >
{
    typedef True type;
};

/****************************************************************************************/
/*                      All the extra stuffed that is being pushed in.                  */
/****************************************************************************************/

/****************************************************************************************/
/*                          Static struct method and std::max                           */
/****************************************************************************************/
struct dummy
{
    template< typename T >
    static const T& retrieveHighestNumber( T& first, T& second )
    {
        return std::max( first, second );
    }
};

/****************************************************************************************/
/*                              Variadic function.                                      */
/****************************************************************************************/
/**
 * @brief
 *
 * @tparam T
 * @param t
 * @return T
 */
template< typename T >
T retrieveNumberDependingOnParameterCount( T t )
{
    t = t; // Removes unused parameter warning.
    return 1;
}
/****************************************************************************************/

template< typename T, typename... Args >
T retrieveNumberDependingOnParameterCount( T first, Args... args )
{
    first = first; // Removes unused parameter warning.
    return 1 + retrieveNumberDependingOnParameterCount( args... );   
}
/****************************************************************************************/

/****************************************************************************************/
/*                                  Variadic struct                                     */
/****************************************************************************************/
/**
 * @brief
 *
 * @tparam T
 */
template < typename... T >
struct VariadicStruct
{
    const static int value = 0;
};
/****************************************************************************************/

template<typename T, typename ... Rest>
struct VariadicStruct< T, Rest ...>
{
    VariadicStruct( const T& first, const Rest& ... rest) : first( first ), rest( rest... )
    {

    }
    
    T first;  

    VariadicStruct< Rest ... > rest;

    const static int value = VariadicStruct< Rest ... >::value + 1;
};
/****************************************************************************************/

/****************************************************************************************/
/*                                  Bit rolling stuff                                   */
/****************************************************************************************/





/**
 * @brief Base TESTER class.
 * 
 * Not used ATM.
 * 
 */
struct BASETESTER
{
    virtual void printer( void ) = 0;
};

/**
 * @brief TESTER struct to simulate "Items".
 * 
 */
struct TESTER : public BASETESTER
{
    /**
     * @brief Data pointer.
     * 
     */
    int* data = nullptr;
    /****************************************************************************************/

    /**
     * @brief Construct a new TESTER object
     * 
     */
    TESTER()
    {
        data = new int[ 3 ];
    }
    /****************************************************************************************/

    /**
     * @brief Construct a new TESTER object with all ints being incoming value.
     * 
     * @param a 
     */
    TESTER( int a )
    {
        data = new int[ 3 ];
        for ( int i = 0; i < 3; i++ )
        {
            data[ i ] = a;
        }
    }
    /****************************************************************************************/

    /**
     * @brief Construct a new TESTER object
     * 
     * @param other 
     */
    TESTER( const TESTER& other ) : TESTER()
    {
        ( *this ) = other;
    }
    /****************************************************************************************/

    /**
     * @brief Construct a new TESTER object
     * 
     * @param other 
     */
    ~TESTER()
    {
        delete [] data;
    }
    /****************************************************************************************/

    /**
     * @brief Calculates total of all int values.
     * 
     * @return int      Total of all int values.
     */
    int total( void )
    {
        return ( data[ 0 ] + data[ 1 ] + data[ 2 ] );
    }
    /****************************************************************************************/

    /**
     * @brief 
     * 
     * @param a 
     */
    void fill( int a )
    {
        for (int i = 0; i < 3; i++)
        {
            data[ i ] = a;
        }
        
    }
    /****************************************************************************************/

    /**
     * @brief 
     * 
     */
    virtual void printer( void ) override
    {

    }
    /****************************************************************************************/

    /**
     * @brief Used to simulate copy-construction or copy-assignment.
     * 
     * @param other 
     * @return TESTER& 
     */
    TESTER& operator=( const TESTER& other )
    {
        for ( int i = 0; i < 3; i++ )
        {
            data[ i ] = other.data[ i ];
        }
        return ( *this );
    }
    /****************************************************************************************/
};

/****************************************************************************************/
/*                                  TAGS.                                               */
/****************************************************************************************/
/**
 * @brief Functions as a tag for being trivially copyable.
 *
 */
struct isTrivial
{
    int a;
};
/****************************************************************************************/

/**
 * @brief Functions as a tag for not being trivially copyable.
 * 
 */
struct notTrivial
{
    int b;
};
/****************************************************************************************/

/**
 * @brief Tag for being a pointer type.
 * 
 */
struct pointerType
{};
/****************************************************************************************/

/**
 * @brief Tag for NOT being a pointer type.
 * 
 */
struct notPointerType
{};
/****************************************************************************************/

/****************************************************************************************/
/**                                 Checking logic.                                     */
/****************************************************************************************/
/**
 * @brief Checks if a type is of type pointer.
 * 
 * @tparam T                Type to check for being pointer.
 */
template< typename T >
struct isPointer
{
    typedef notPointerType type;
};
/****************************************************************************************/

/**
 * @brief If a type is a pointer.
 * 
 * @tparam                  Type being checked for being a pointer.
 */
template< typename T >
struct isPointer< T* >
{
    typedef pointerType type;
};
/****************************************************************************************/

/**
 * @brief Checks if a type is a pointer TO a pointer. ( of type ** )
 * 
 * @tparam T                Type being checked for being **.
 */
template< typename T >
struct isPointerToPointer
{
    typedef notPointerType type;
};
/****************************************************************************************/

/**
 * @brief Checks to see if a type is pointer TO a pointer. ( of type ** )
 * 
 * @tparam T                Type being checked for being **.
 */
template< typename T > 
struct isPointerToPointer< T* > 
{
    typedef typename isPointer< T >::type type;
};
/****************************************************************************************/

/**
 * @brief Takes in two types, and typedef's one depending on bool.
 * @brief ( works as "if_then_else")
 * 
 * @tparam  B           Bool for deciding either of the types.
 * @tparam  T           The TRUE type ( the one typedeffed here. ).
 * @tparam  U           The FALSE type.
 */
template< bool B, typename T, typename U >
struct isCopyable
{
    typedef T someType;
};
/****************************************************************************************/

/**
 * @brief Template specialization that actually typedef's the FALSE type.
 * @brief Works as "if_then_else".
 * 
 * @tparam T            The TRUE type.
 * @tparam U            The FALSE type ( the one typedeffed here. ).
 */
template< typename T, typename U >
struct isCopyable< false, T, U >
{
    typedef U someType;
};
/****************************************************************************************/

/**
 * @brief Takes in a type, and typedefs checked as either isTrivial or notTrivial.
 * @brief In other words, it takes a type, and then uses if_then_else.
 * 
 * @tparam T            The type to check for being trivially copyable or not.
 */
template< typename T >
struct checkCopyable
{
    typedef typename isCopyable< std::is_trivially_copyable< T >::value, isTrivial, notTrivial >::someType checked;
};
/****************************************************************************************/

/**
 * @brief namespace for storing algorithms.
 * 
 */
namespace algorithms
{
    /**
     * @brief Copy algorithm using iterators.
     * 
     * @tparam Iterator     The iterator type being used.
     * @param begin         The iterator pointing to start of data copying from.
     * @param end           The iterator pointing to end of data copying from.
     * @param to            The iterator pointing to the data copying to.
     */
    template< typename Iterator >
    void copyWithIterator( Iterator begin, Iterator end, Iterator to )
    {
        for (; begin != end; ++begin, ++to )
        {
            ( *to ) = ( *begin );
        }
        
    }
    /****************************************************************************************/

    /**
     * @brief Iterates over pointers. So begin is a pointer to a pointer.
     * 
     * @tparam Iterator     Iterator points to an array of pointers.
     * @param begin         Iterator that points to a pointer, start pointer to be copied.
     * @param end           Iterator that points to a pointer, end pointer to be copied.
     * @param to            Iterator that points to a pointer, "to" location to copy to.
     */
    template< typename Iterator, typename OtherIterator >
    void copyWithIterator( Iterator begin, Iterator end, OtherIterator to, pointerType )
    {     
        if constexpr( ( std::is_same< typename Iterator::pointer_pointer_type, void** >::value ) 
                   && ( std::is_same< typename OtherIterator::pointer_pointer_type, void**>::value ) )
        {
            /**
             * @brief Comes here, if both copee and copier is of void**.
             * 
             * objectType is then defined as the type being pointed to by begin/end. = void**....
             * This situation is SAD and unfortunately undhandled...
             */
            //using objectType = typename Iterator::value_type;

            for (; begin != end; begin++, to++ )
            {
                // Well, if we end up here, we can't do anything. Screw u.s
            }
        }
        else if constexpr( ( !( std::is_same< typename Iterator::pointer_pointer_type, void**>::value ) )  
                          && ( std::is_same<typename OtherIterator::pointer_pointer_type, void** >::value ) )
        {
            if constexpr ( std::is_same< Item*, typename Iterator::pointer_type >::value )
            {                
                copyAlgoSpecificToItems( begin, end, to );     
            }
            else
            {
                    /**
                 * @brief Comes here if begin and end is != void**, and to is of void**.
                 * 
                 * objectType is then defined as the type being pointed to by begin/end.
                 * This could be int, float, Item, Player. Any object type.
                 */
                using objectType = typename Iterator::value_type;

                /**
                 * @brief It goes in a loop from begin to end.
                 * 
                 * Every loop-round, it generates a new object of OBJECTTYPE.
                 * The object created takes in a similar object, and then copy-constructs it.
                 * This new object is being catched by the void* in "to".
                 * 
                 */
                for (; begin !=end; begin++, to++)
                {
                    ( *to ) = new objectType( *begin );
                }
            }
            
            
            
        }
        else if constexpr( ( ( std::is_same< typename Iterator::pointer_pointer_type, void**>::value ) )  &&                              ( !( std::is_same<typename OtherIterator::pointer_pointer_type, void** >::value ) ) )
        {
            if constexpr ( std::is_same< Item*, typename OtherIterator::pointer_type >::value )
            {                
                copyAlgoSpecificToItems( begin, end, to );     
            }
            else
            {
                    /**
                 * @brief Comes here if begin/end is of void**, but to is of other type.
                 * 
                 * objectType is here defined as the type being pointed to, by the iterator "to".
                 * 
                 * This type, again, could be of any type.
                 * 
                 */
                using objectType = typename OtherIterator::value_type;

                /**
                 * @brief It loops again, but this time, extracts the type being pointed to by the void*.
                 * Here, a new object of OBJECTTYPE is created, and uses the one pointed to by begin.
                 * 
                 */
                int count = 0;
                for ( ; begin != end; begin++, to++ )
                {
                    ( to.at( count ) ) = new objectType( * ( reinterpret_cast< objectType* >( *begin ) ) );
                }
            }
            
            
        }
        else
        {
            /**
             * @brief It comes here, if neither of the iterators are of void**.
             * Assumably, only Containers of same type or void* types are being copied.
             * IT CANNOT HANDLE GenericContainer< int* > = GenericContainer< TESTER* >.
             * This is a known problem, but just don't misuse the function.
             * 
             */
            if constexpr ( std::is_same< Item*, typename Iterator::pointer_type >::value )
            {                
                copyAlgoSpecificToItems( begin, end, to );     
            }
            else
            {
                // Doesn't have the member method.
                using objectType = typename Iterator::value_type;

                int count = 0;
                for (; begin != end; begin++, count++ )
                {
                    // It will provide you with a compiler error if you are doing something illegal.
                    //( to.at( count ) ) = new objectType( *begin ); 
                    // Below, however, won't.
                    ( reinterpret_cast< void*& >( to.at( count ) ) ) = new objectType( *begin );
                }
            }
        }
    }
    /****************************************************************************************/

    /**
     * @brief Being called by copyUsingTag. Actually copies.
     * 
     * @tparam T            The type that is to be copied.
     * @param begin         Pointer to start of data being copied.
     * @param end           Pointer to end of data being copied.
     * @param to            Pointer to data copying to.
     */
    template< typename T >
    void actualCopying( T* begin, T* end, T* to, isTrivial )
    {
        memcpy( to, begin, sizeof( T ) * ( end - begin ) );
    }
    /****************************************************************************************/

    /**
     * @brief Being called by copyUsingTag. Calls copyWithIterator.
     * 
     * @tparam T            The type that is to be copied,
     * @param begin         Pointer to start of data being copied.
     * @param end           Pointer to end of data being copied.
     * @param to            Pointer to data copying to.
     */
    template< typename T > 
    void actualCopying( T* begin, T* end, T* to, notTrivial )
    {
        copyWithIterator( GenericIterator< T >( begin ), GenericIterator< T >( end ), GenericIterator< T >( to ) );
    }
    /****************************************************************************************/

    /**
     * @brief Calls a function that actually copies, depending on the tag.
     * 
     * Calls actualCopying with isTrivial, if T is trivially copyable.
     * Calls actualCopying with notTrivial, if T isn't trivially copyable.
     * 
     * @tparam T            The type that are being copied, and checked for being trivial.
     * @param begin         Pointer start to data being copied.
     * @param end           Pointer end to data being copied.
     * @param to            Pointer to data copying to.
     */
    template< typename T > 
    void copyUsingTag( T* begin, T* end, T* to ) 
    {
        actualCopying( begin, end, to, typename checkCopyable< T* >::checked() ); 
    }
    /****************************************************************************************/


    template< typename T, typename U >
    void copySmart( T* begin, T* end, U* to )
    {
        if constexpr( std::is_same< typename isPointerToPointer< T* >::type, pointerType >::value )
        {
            copyWithIterator( GenericIterator< T >( begin ), GenericIterator< T >( end ), GenericIterator< U >( to ), pointerType() );
        }
        else
        {
            copyUsingTag( begin, end, to );
        }
    }
    /****************************************************************************************/