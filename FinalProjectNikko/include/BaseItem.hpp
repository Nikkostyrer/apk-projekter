/**
 * General BaseItem interface.
 * 
 * This works as a common object, that can be used as a pointer type.
 * Derived classes are the actual objects.
 * Usage of VTables.
 * 
 * ( Interface needs to have all the wished methods that items can have. i.e. add later. )
 */
class BaseItem
{
    // Contains a pointer to the owner of the object.
    void* owner = nullptr; // <-- nullptr.
    
    // Buy price is the price from the store/dealer.
    double buyPrice = 0; // <-- Assignment initialization.

    // Sell price is the price when sold.
    double sellPrice{ 4.2 }; // <-- The other one.

    // Public methods serve as the actual interface.
  public:
    virtual double getBuyPrice( void ) const = 0;
    virtual double getSellPrice( void ) const = 0;
    virtual void setBuyProce( double ) = 0;
    virtual void setSellPrice( double ) = 0;
};