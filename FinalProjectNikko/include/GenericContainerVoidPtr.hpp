#ifndef __GENERIC_CONTAINER_VOID_PTR_HPP__
#define __GENERIC_CONTAINER_VOID_PTR_HPP__

#include "GenericContainer.hpp"
#include <any>


template<>
class GenericContainer< void* >
{
protected:
    /****************************************************************************************/
    /*                                   Class typedefs.                                    */
    /****************************************************************************************/
    typedef GenericIterator< void* > iterator;
    typedef const GenericIterator< void* > const_iterator;

    /****************************************************************************************/
    /*                                   Class members.                                     */
    /****************************************************************************************/
    // Pointer for storing data.
    void** data = nullptr;

    // The number of items in the container currently.
    int count;

    // The space in the container.
    int capacity;

public:
    /****************************************************************************************/
    /*                                   Class constructors.                                */
    /****************************************************************************************/
    /**
     * Creates an empty container with 1 capacity.
     * Basic default constructor.
     */
    GenericContainer() 
    {
        capacity = 0.0; // <-- Implicit conversion.
        count = NULL;   // <-- Implicit conversion.
        data = nullptr;
    }
    /****************************************************************************************/

    /**
     * Creates an empty container with predefined capacity.
     * Delegating constructor.
     * 
     * @param[in]  capacityIn               The capacity of the container.
     */
    GenericContainer( int capacityIn ) : GenericContainer()
    {
        try
        {
            data = new void*[ capacityIn ];
            // for (int i = 0; i < capacityIn; i++ )
            // {
            //     data[ i ] = NULL;
            // }
            
            capacity = capacityIn;
        }
        catch( const std::exception& e )
        {
            std::cerr << "Bad allocation of pointer array\n";
            std::cerr << "Std exception " << e.what() << "\n";
            delete data;
            capacity = 0;
            throw;
        }
        
    }
    /****************************************************************************************/

    /**
     * Creates a container at defined capacity full of equal objects.
     * Explicit constructor.
     * 
     * @param[in] capacityIn                The capacity of the container.
     * @param[in] toFill                    The object to copy into all slots.
     * 
     * TAG: BROKEN - FIX this
     */
    template< typename T >
    GenericContainer( int capacityIn, const T toFill ) : GenericContainer( capacityIn )
    {
        count = capacity = capacityIn;
        algorithms::fill( begin(), end(), toFill );
    }
    /****************************************************************************************/

    /**
     * Creates a container with predefined data, datasize and capacity.
     * 
     * @param[in] dataIn                    The pre-created data.
     * @param[in] countIn                   The number of object in the container.
     * @param[in] capacityIn                The capacity of the container.
     */
    GenericContainer( void** dataIn, int countIn, int capacityIn ) : data( dataIn ), count( countIn ), capacity( capacityIn)
    {}
    /****************************************************************************************/

    /**
     * Creates a container similar to the one supplied.
     * 
     * @param[in] copyFrom                  The container to COPY from.
     */
    template< typename T >        
    GenericContainer( GenericContainer< T >& copyFrom ) : GenericContainer()
    {
        ( *this ) = copyFrom;
    }
    /****************************************************************************************/

    
    /**
     * Moves everything from applied container here.
     * 
     * @param[in]                           The container to move data from.
     */
    template< typename T >
    GenericContainer( GenericContainer< T >&& moveFrom ) : GenericContainer()
    {
        ( *this ) = ( std::forward< GenericContainer< T > >( moveFrom ) );
    }
    /****************************************************************************************/

    /**
     * De-allocates the objects pointed to by the pointer.
     * If a smart pointer is implemented, it can invoke the destructor of that one.
     */
    ~GenericContainer()
    {
        capacity = 0;
        count = 0;
        // for ( int i = 0; i < count; i++ )
        // {   
        //     delete data[ i ];
        // }
        // delete[] data;
    }


    /****************************************************************************************/
    /*                                   Operator overloads.                                */
    /****************************************************************************************/
    /**
     * Copy assigment overload.
     * Copies everything from target to here.
     * 
     * @param[in] copyFrom                  The object to copy data from.
     * @return                              A reference to the object that invoked this.
     */
    template< typename T >
    GenericContainer< void* >& operator = ( GenericContainer< T >& copyFrom )
    {
        GenericContainer< T > temp( copyFrom.getCapacity() );

        //algorithms::copySmart( copyFrom.begin().ptr, copyFrom.end().ptr, temp.begin().ptr );
        // or below
        algorithms::copySmart( reinterpret_cast< T* >( copyFrom.data ), reinterpret_cast<T*>( &copyFrom.data[ copyFrom.count ] ), temp.front() );

        temp.count = copyFrom.count;

        swap( temp );

        return *this;
    }
    /****************************************************************************************/

    /**
     * Move assignment overload.
     * Moves everything from target to here.
     * 
     * @param[in] moveFrom                  The object to move data from.
     * @return                              A reference to the object that invokes the move assignment.
     */
    template< typename T >
    GenericContainer& operator = ( GenericContainer< T >&& moveFrom )
    {
        // Creates temp object with the capacity of the one moving from.
        GenericContainer< void* > temp( moveFrom.getCapacity() );        
        
        // This creates a pointer to data, that needs to be deleted aswell. ^^^^
        // We set data to look at that, so that temp deletes it later.
        data = temp.front();
        // Get temp to point to the data of the movable object.
        temp.data = moveFrom.front();
        // Retrieve that count aswell.
        temp.count = moveFrom.getCount();
        // Clear the movable objects data.
        moveFrom.clear();
        swap( temp );  
        return ( *this );
    }
    /****************************************************************************************/

    /**
     * Dereference operator overload.
     * 
     * @return                              A reference to the object dereferenced to.
     */
    void*& operator * ()
    {
        return *data;
    }
    /****************************************************************************************/

    /**
     * Index operator overload.
     * 
     * @param[in] index                     Index number requested.
     * @return                              Reference to the object requested.
     */
    void*& operator [] ( int index )
    {      
        if( index < count )
        {
            return data[ index ];
        }
        else
        {
            return data[ 0 ];
        }
        
    }
    /****************************************************************************************/


    /****************************************************************************************/
    /*                                   Class methods.                                     */
    /****************************************************************************************/
    /**
     * Gets object in front.
     * 
     */
    void** front( void )
    {
        return reinterpret_cast< void** >( data );
    }
    /****************************************************************************************/

    void setCount( int toSet )
    {
        count = toSet;
    }
    
    void** back( void )
    {
        return reinterpret_cast< void** >( &( data[ count ] ) );
    }
    /****************************************************************************************/
    
    /**
     * Clears information.
     * 
     */
    void clear( void )
    {
        data = nullptr;
        
        capacity = 0;
        count = 0;
    }
    /****************************************************************************************/

    /**
     * Checks wether the container is empty or not.
     * 
     * @return                              Boolean, empty or not.
     */
    bool empty( void ) const
    {
        if ( count != 0 )
        {
            return true;
        }
        return false;
    }
    /****************************************************************************************/

    /**
     * Returns the count in the container.
     * 
     * @return                              The number of objects in the container.
     */
    int size( void ) const
    {
        return count;
    }
    /****************************************************************************************/

    /**
     * Retrieves the theoritical max size of the container.
     * 
     * @return                              The theoretic max of the container.
     */
    auto max_size( void )
    {
        return std::numeric_limits< void* >::max();
    }
    /****************************************************************************************/

    /**
     * Works on a container at an index value.
     * 
     * @param[in] index                     The index that is to be accessed.
     * @return                              A reference to the object at the index.
     */
    void* at( int index )
    {
        return data[ index ];
    }
    /****************************************************************************************/

    /**
     * Pushes an object onto the back.
     * 
     */
    void pushBack( void* toPush )
    {
        if ( capacity > 0 )
        {
            if( count == capacity )
            {
                void** temp = new void*[ capacity * 2 ];
                for ( int i = 0; i < ( capacity * 2 ); i++ )
                {
                    if ( i < capacity )
                    {
                        temp[ i ] = data[ i ];
                    }
                    else
                    {
                        temp[ i ] = nullptr;
                    }
                }
                delete [] data;
                data = temp;
                temp = nullptr;
                data[ count ] = toPush;
                count++;
                capacity = capacity * 2;
                
                
            }
            else
            {
                int zero = 0;
                // Check for the highest number between zero and count. Because, why not.
                data[ ( dummy::retrieveHighestNumber( zero, count ) ) ] = toPush;
                count++;


            }
        }
        else
        {
            int number = retrieveNumberDependingOnParameterCount( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 );
            data = new void*[ number ];
            data[ count ] = toPush;
            for ( int i = 1; i < capacity; i++ )
            {
                data[ i ] = nullptr;
            }
            
            capacity = number;
            count++;
        } 
    }
    /****************************************************************************************/
    
    /**
     * Pops an object in the back.
     * 
     */
    void popBack( void )
    {
        if( count > 0 )
        {
            delete data[ count - 1 ];
            data[ ( count - 1 ) ] = nullptr;
            count--;
        }
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    bool reserve( int slots )
    {
        if( slots > capacity )
        {
                void** temp = new void*[ ( slots * 2 ) ];
                for ( int i = 0; i < slots; i++ )
                {
                    if ( i < capacity )
                    {
                        temp[ i ] = data[ i ];
                    }
                    else
                    {
                        temp[ i ] = nullptr;
                    }
                }
                delete [] data;
                data = temp;
                temp = nullptr;
                capacity = slots;
            return true;
        }
        return false;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    int getCount( void ) const
    {
        return count;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    int getCapacity( void ) const
    {
        return capacity;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    bool shrink_to_fit( void )
    {
        // GenericContainer< void* > temp( count );

        // swap( temp );
        // // After swap, data is null, and count is null.
        // count = temp.count;

        // algorithms::copySmart( temp.front(), ( temp.front() + count ), front() );
        void** temp = new void*[ count ];
        for ( int i = 0; i < count; i++ )
        {
            temp[ i ] = data[ i ];
        }
        delete [] data;
        data = temp;
        temp = nullptr;
        capacity = count;


        

        return true;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    void swap( GenericContainer< void* >& swapFrom )
    {
        // Stores old data.
        void** tempPtr = data;
        int tempCapacity = capacity;
        int tempCount = count;

        // Saves swappable data.
        data = swapFrom.data ;
        capacity = swapFrom.capacity;
        count = swapFrom.count;

        // Swaps to the other one aswell.
        swapFrom.data = tempPtr;
        swapFrom.capacity = tempCapacity;
        swapFrom.count = tempCount;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    iterator begin( void )
    {
        return data;
    }
    /****************************************************************************************/
    
    /**
     * @brief 
     * 
     * @return iterator 
     */
    iterator end( void )
    {
        return &( data[ count ] );
    }
    /****************************************************************************************/

    /**
     * @brief 
     * 
     * @return iterator 
     */
    iterator endCap( void )
    {
        return &( data[ capacity ] );
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    const_iterator cbegin( void ) const
    {
        return data;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    const_iterator cend( void ) const
    {
        return &( data[ capacity ] );
    }
    /****************************************************************************************/

    void printInfoForDebug( void ) const
    {
        std::cout << "\nMy capacity is: " << capacity << " and my count is: " << count << " ";
        if ( data != nullptr )
        {
            std::cout << "my data is different from nullptr!\n";
            std::cout << "And data is: ";
            for ( int i = 0; i < count; i++ )
            {
                std::cout << data[ i ] << " ";
            }
            
        }
        else
        {
            std::cout << "my data is equal nullptr...\n";
        }
        std::cout << "\n";
    }
};
/****************************************************************************************/

template< typename T >
class GenericContainer< T* > : public GenericContainer< void* >
{
    using GenericContainer< void* >::GenericContainer;
    using Base = GenericContainer< void* >;

    /**
     * De-allocates the objects pointed to by the pointer.
     * If a smart pointer is implemented, it can invoke the destructor of that one.
     */
    /****************************************************************************************/
    /*                                   Class typedefs.                                    */
    /****************************************************************************************/
    typedef GenericIterator< T* > iterator;
    typedef const GenericIterator< T* > const_iterator;



public:
    ~GenericContainer< T* >()
    {
        capacity = 0;
        count = 0;
        // std::cout << "Calling destructor in T* container\n";
        // for ( int i = 0; i < count; i++ )
        // {   
        //     delete data[ i ];
        // }
        //delete [] ( reinterpret_cast< T** >( data ) );
        // std::cout << "Calling destructor in T* container AGAIN\n";
    }

    /**
     * Dereference operator overload.
     * 
     * @return                              A reference to the object dereferenced to.
     */
    T& operator * ()
    {
        return ( * ( reinterpret_cast< T* >( *data ) ) );
    }
    /****************************************************************************************/

    /**
     * Index operator overload.
     * 
     * @param[in] index                     Index number requested.
     * @return                              Reference to the object requested.
     */
    T& operator [] ( int index )
    {
        if( index < count )
        {
            return ( * ( reinterpret_cast< T* >( data[ index ] ) ) );
        }
        else
        {
            return ( * ( reinterpret_cast< T* >( data[ 0 ] ) ) );
        }
        
    }
    /****************************************************************************************/

    /**
     * @brief Construct a new Generic Container object
     * 
     * @tparam U 
     * @param copyFrom 
     */
    template< typename U >
    GenericContainer( GenericContainer< U >& copyFrom ) : GenericContainer()
    {
        ( *this ) = copyFrom;
    }
    /****************************************************************************************/

    /**
     * @brief 
     * 
     * @tparam U 
     * @param copyFrom 
     * @return GenericContainer& 
     */
    template< typename U >
    GenericContainer& operator = ( GenericContainer< U >& copyFrom )
    {
        GenericContainer< T* > temp( copyFrom.getCapacity() );
        //algorithms::copySmart( copyFrom.begin().ptr, copyFrom.end().ptr, temp.begin().ptr );
        // or below
        algorithms::copySmart( reinterpret_cast< U* >( copyFrom.front() ), reinterpret_cast< U* >( copyFrom.back() ), reinterpret_cast< T** >( temp.front() ) );

        temp.count = copyFrom.getCount();

        swap( temp );
  
        return *this;
    }
    /****************************************************************************************/


    /**
     * Moves everything from applied container here.
     * 
     * @param[in]                           The container to move data from.
     */
    template< typename U >
    GenericContainer( GenericContainer< U >&& moveFrom ) : GenericContainer()
    {
        ( *this ) = ( std::forward< GenericContainer< U > >( moveFrom ) );
    }
    /****************************************************************************************/

    /**
     * Move assignment overload.
     * Moves everything from target to here.
     * 
     * @param[in] moveFrom                  The object to move data from.
     * @return                              A reference to the object that invokes the move assignment.
     */
    template< typename U >
    GenericContainer& operator = ( GenericContainer< U >&& moveFrom )
    {
        // Creates temp object with the capacity of the one moving from.
        GenericContainer< T* > temp( moveFrom.getCapacity() );
        // This creates a pointer to data, that needs to be deleted aswell. ^^^^
        // We set data to look at that, so that temp deletes it later.
        data = temp.data;
        // Get temp to point to the data of the movable object.
        temp.data = moveFrom.front();
        // Retrieve that count aswell.
        temp.count = moveFrom.getCount();
        // Clear the movable objects data.
        moveFrom.clear();
        swap( temp );  

        return *this;
    }
    /****************************************************************************************/


    /**
     * Clears information.
     * 
     */
    void clear( void )
    {
        Base::clear();
    }
    /****************************************************************************************/
    
    /**
     * Checks wether the container is empty or not.
     * 
     * @return                              Boolean, empty or not.
     */
    bool empty( void ) const
    {
        return Base::empty();
    }
    /****************************************************************************************/
    
    /**
     * Returns the count in the container.
     * 
     * @return                              The number of objects in the container.
     */
    int size( void ) const
    {
        return Base::size();
    }
    /****************************************************************************************/
    
    /**
     * Retrieves the theoritical max size of the container.
     * 
     * @return                              The theoretic max of the container.
     */
    auto max_size( void )
    {
        return std::numeric_limits< T* >::max();
    }
    /****************************************************************************************/

    /**
     * Pushes an object onto the back.
     * 
     */
    void pushBack( void* toPush )
    {
        Base::pushBack( toPush );
    }
    /****************************************************************************************/
    
    /**
     * Pops an object in the back.
     * 
     */
    void popBack( void )
    {
        Base::popBack();
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    bool reserve( int slots )
    {
        return Base::reserve( slots );
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    int getCount( void ) const
    {
        return Base::getCount();
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    int getCapacity( void ) const
    {
        return Base::getCapacity();
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    bool shrink_to_fit( void )
    {
        return Base::shrink_to_fit();
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    void swap( GenericContainer< void* >& swapFrom )
    {
        Base::swap( swapFrom );
    }
    /****************************************************************************************/
};




#endif //__GENERIC_CONTAINER_VOID_PTR_HPP__