#ifndef __GENERIC_ITERATOR_HPP__
#define __GENERIC_ITERATOR_HPP__

#include <iterator>
#include <iostream>


/**
 * @brief Generic Iterator class.
 * 
 * @tparam T 		The type that is being "pointed"/"iterated" to.
 */
template< typename T >
class GenericIterator : public std::iterator< std::input_iterator_tag, T >
{
  public:
    T* ptr = nullptr; // <-- nullptr.

	
	using value_type = T;
	using pointer_type = T*;
	using pointer_pointer_type = T**;
	using reference_type = T&;
	using difference_type = int;



	/**
	 * @brief Construct a new Generic Iterator object
	 * 
	 * @param in 
	 */
    GenericIterator( T* in )
    {
        ptr = in;
    }

	/**
	 * @brief Construct a new Generic Iterator object
	 * 
	 * @param itIn 
	 */
    GenericIterator( const GenericIterator& itIn )
	{
		ptr = itIn.ptr;
	}

	/**
	 * @brief Increment iterator.
	 * 
	 * @return GenericIterator& 
	 */
	GenericIterator& operator ++ ()
	{
		ptr++;
		return *this;
	}

	/**
	 * @brief Increment iterator.
	 * 
	 * @return GenericIterator 
	 */
	GenericIterator operator++( int )
	{
		GenericIterator< T > tmp( *this );
		operator++();
		return tmp;
	}

	/**
	 * @brief Increment iterator by value.
	 * 
	 * @param increment 
	 * @return GenericIterator& 
	 */
	GenericIterator& operator+( int increment )
	{		
		ptr = ptr + increment;
	}

	/**
	 * @brief Decrement iterator by value.
	 * 
	 * @param decrement 
	 * @return GenericIterator& 
	 */
	GenericIterator& operator-( int decrement )
	{
		ptr = ptr + decrement;
	}

	/**
	 * @brief Compare iterators.
	 * 
	 * @param right 
	 * @return true 
	 * @return false 
	 */
	bool operator==( const GenericIterator& right ) const
	{
		return ptr == right.ptr;
	}

	/**
	 * @brief Compare iterators.
	 * 
	 * @param right 
	 * @return true 
	 * @return false 
	 */
	bool operator!=( const GenericIterator& right ) const
	{
		return ptr != right.ptr;
	}

	/**
	 * @brief Return object with index operator.
	 * 
	 * @param index 
	 * @return T& 
	 */
	T& operator[](int index )
	{
		return ptr[ index ];
	}

	/**
	 * @brief Return with dereference operator.
	 * 
	 * @return T& 
	 */
	T& operator*()
	{
		return *ptr;
	}
};



/**
 * @brief Create Iterator to handle void pointers.
 * 
 * Dereferenced type need to be of **, so that it doesn't return a pointer to Item*, but a reference to the actual 			object.
 * 
 * @tparam T being the pointer type now.
 */
template<>
class GenericIterator< void* > : public std::iterator< std::input_iterator_tag, void* >
{
  public:
    /****************************************************************************************/
	/*								Pointer to type											*/
    /****************************************************************************************/
    void** ptr = nullptr; // <-- nullptr.

    /****************************************************************************************/
	/*								Iterator sub-types.										*/
    /****************************************************************************************/
    using value_type = void;
	using pointer_type = void*;
	using pointer_pointer_type = void**;
	using reference_type = void*&;
	using difference_type = int;

    /****************************************************************************************/
	/*								Iterator constructors.									*/
    /****************************************************************************************/
    /**
     * @brief Construct a
     * new Generic
     * Iterator object
     *
     * @param in
     */
    GenericIterator( void** in )
    {
        ptr = in;
    }
    /****************************************************************************************/

    /**
	 * @brief Construct a new Generic Iterator object
	 * 
	 * @param itIn 
	 */
    GenericIterator( const GenericIterator& itIn )
	{
		ptr = itIn.ptr;
	}
    /****************************************************************************************/

    /****************************************************************************************/
	/*								Iterator methods.										*/
    /****************************************************************************************/
    /**
	 * @brief 
	 * 
	 * @return GenericIterator& 
	 */
	GenericIterator& operator ++ ()
	{
		ptr++;
		return *this;
	}
    /****************************************************************************************/

    /**
	 * @brief 
	 * 
	 * @return GenericIterator 
	 */
	GenericIterator operator++( int )
	{
		GenericIterator< void* > tmp( *this );
		operator++();
		return tmp;
	}
    /****************************************************************************************/

    /**
	 * @brief 
	 * 
	 * @param increment 
	 * @return GenericIterator& 
	 */
	GenericIterator& operator+( int increment )
	{		
		ptr = ptr + increment;
		return ( *this );
	}
    /****************************************************************************************/

    /**
	 * @brief 
	 * 
	 * @param decrement 
	 * @return GenericIterator& 
	 */
	GenericIterator& operator-( int decrement )
	{
		ptr = ptr + decrement;
		return ( *this );
	}
    /****************************************************************************************/

    /**
	 * @brief 
	 * 
	 * @param right 
	 * @return true 
	 * @return false 
	 */
	bool operator==( const GenericIterator& right ) const
	{
		return ptr == right.ptr;
	}
    /****************************************************************************************/

    /**
	 * @brief 
	 * 
	 * @param right 
	 * @return true 
	 * @return false 
	 */
	bool operator!=( const GenericIterator& right ) const
	{
		return ptr != right.ptr;
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @param index 
	 * @return auto& 
	 */
    void*& at( int index )
	{
		return ptr[ index ];
	}
    /****************************************************************************************/

    /**
	 * @brief 
	 * 
	 * @param index 
	 * @return auto 
	 */
	auto operator[]( int index )
	{
		return ( ptr[ index ] );
	}
    /****************************************************************************************/

    /**
	 * @brief 
	 * 
	 * @return void*& 
	 */
	void*& operator*( )
	{
		return *ptr;
	}
    /****************************************************************************************/
};



/**
 * @brief Create Iterator to handle pointer pointers.
 * TODO: Read thoughts below.
 * 
 * Dereference and index operator does not return the pointer being pointed to.
 * It, instead, returns the ACTUAL object, hence the ** in the deref. 
 * This MAY NOT be the most intuitive, or, the most smart thing, but it's implemented with this project in mind.
 *  
 * 
 * @tparam T being the pointer type now.
 */
template< typename T >
class GenericIterator< T* > : public std::iterator< std::input_iterator_tag, T* >
{
  public:
    /****************************************************************************************/
	/*								Iterator object-pointer.								*/
    /****************************************************************************************/
    T** ptr = nullptr; // <-- nullptr.

    /****************************************************************************************/
	/*								Iterator sub-types.										*/
    /****************************************************************************************/
    using value_type = T;
	using pointer_type = T*;
	using pointer_pointer_type = T**;
	using reference_type = T*&;
	using difference_type = int;

    /****************************************************************************************/
	/*								Iterator constructors.									*/
    /****************************************************************************************/
	/**
	 * @brief Construct a new Generic Iterator object
	 * 
	 * @param in 
	 */
    GenericIterator( T** in )
    {
        ptr = reinterpret_cast< T** >( in ); // whae
    }
    /****************************************************************************************/

	/**
	 * @brief Construct a new Generic Iterator object
	 * 
	 * @tparam U 
	 * @param itIn 
	 */
    template< typename U >
    GenericIterator( const GenericIterator< U >& itIn )
	{
		ptr = reinterpret_cast< T** >( itIn.ptr );
	}
    /****************************************************************************************/

    /****************************************************************************************/
	/*								Iterator methods.										*/
    /****************************************************************************************/
    /**
     * @brief
     *
     * @return
     * GenericIterator&
     */
    GenericIterator& operator ++ ()
	{
		ptr++;
		return *this;
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @return GenericIterator 
	 */
    GenericIterator operator++( int )
	{
		GenericIterator< T* > tmp( *this );
		operator++();
		return tmp;
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @param increment 
	 * @return GenericIterator& 
	 */
    GenericIterator& operator+( int increment )
	{		
		ptr = ptr + increment;
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @param decrement 
	 * @return GenericIterator& 
	 */
    GenericIterator& operator-( int decrement )
	{
		ptr = ptr + decrement;
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @param right 
	 * @return true 
	 * @return false 
	 */
    bool operator==( const GenericIterator& right ) const
	{
		return ptr == right.ptr;
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @param right 
	 * @return true 
	 * @return false 
	 */
    bool operator!=( const GenericIterator& right ) const
	{
		return ptr != right.ptr;
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @param index 
	 * @return auto& 
	 */
    T*& at( int index )
	{
		return ptr[ index ];
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @param index 
	 * @return auto 
	 */
    T& operator[]( int index )
	{
		return ( * ( ptr[ index ] ) );
	}
    /****************************************************************************************/

	/**
	 * @brief 
	 * 
	 * @return auto& 
	 */
    auto& operator*()
	{
		return **ptr; // whae
	}
    /****************************************************************************************/
};



template< typename Iterator >
struct iterator_types
{
	using diff_type = typename Iterator::diff_type;
	using value_type = typename Iterator::value_type;
	using pointer_type = typename Iterator::pointer_type;
	using reference_type = typename Iterator::reference_type;
};








#endif //__GENERIC_ITERATOR_HPP__