#ifndef __GENERIC_CONTAINER_VOID_PTR_TEST_HPP__
#define __GENERIC_CONTAINER_VOID_PTR_TEST_HPP__

#include "GenericContainerVoidPtr.hpp"
#include "algorithms.hpp"
#include "../../../apk_alexander/include/Drug.hpp"
#include "../../googletest/googletest/src/gtest-all.cc"
#include "GenericContainerTest.hpp"

/****************************************************************************************/
/*                                      Void* tests.                                    */
/****************************************************************************************/
TEST( GenericVoidPtr, Constructors )
{
    GenericContainer< void* > first;

    ASSERT_EQ( first.getCount(), 0 );
    ASSERT_EQ( first.getCapacity(), 0 );
    ASSERT_EQ( first.front(), nullptr );

    GenericContainer< void* > second( 10 );

    ASSERT_EQ( second.getCount(), 0 );
    ASSERT_EQ( second.getCapacity(), 10 );
    ASSERT_NE( second.front(), nullptr );

    GenericContainer< void* > third( 15, 2 );
    int** testInt = reinterpret_cast< int** >( third.front() );
    for ( int i = 0; i < 15; i++ )
    {
        EXPECT_EQ( ( **testInt ), 2 );
    }

    GenericContainer< void* > fourth( 10, TESTER( 5 ) );
    TESTER** testTester = reinterpret_cast< TESTER** >( fourth.front() );
    for ( int i = 0; i < fourth.getCount(); i++ )
    {
        EXPECT_EQ( ( **testTester ).total(), 15 );
    }


    GenericContainer< int* > fifth( 10, 2 );
    int** intTestz = reinterpret_cast< int** >( fifth.front() );
    for ( int i = 0; i < fifth.getCount(); i++ )
    {
        EXPECT_EQ( **intTestz, 2 );   
    }

    GenericContainer< void* > changable( reinterpret_cast< void** >( fifth.front() ), fifth.getCount(), 
    fifth.getCapacity() );
    fifth.clear();

    EXPECT_EQ( changable.getCapacity(), 10 );
    EXPECT_EQ( changable.getCount(), 10 );
    int** otherIntTestz = reinterpret_cast< int** >( changable.front() );
    for ( int i = 0; i < changable.getCount(); i++ )
    {
        EXPECT_EQ( **otherIntTestz, 2 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, CopyConstructorFromTESTERToVoidPtr )
{
    GenericContainer< TESTER* > others( 10, TESTER( 5 ) );
    GenericContainer< void* > copee( others );

    EXPECT_EQ( others.getCapacity(), 10 );
    EXPECT_EQ( others.getCount(), 10 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 10 );



    GenericIterator< TESTER* > otherIt( reinterpret_cast< TESTER** >( others.front() ) );
    GenericIterator< TESTER* > copeeIt( reinterpret_cast< TESTER** >( copee.front() ) );

    
    EXPECT_NE( reinterpret_cast< int** >( others.front() ), reinterpret_cast< int** >( copee.front() ) );
    
    EXPECT_NE( reinterpret_cast< int* >( * ( others.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < others.getCount(); i++ )
    {
        EXPECT_EQ(  ( *otherIt ).total(), 15 );
    }


    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *copeeIt ).total(), 15 );
    }    
}
/****************************************************************************************/

TEST( GenericVoidPtr, CopyConstructorFromTESTERToVoidPtrToTESTER )
{
    GenericContainer< TESTER* > others( 10, TESTER( 5 ) );
    GenericContainer< void* > copee( others );
    GenericContainer< TESTER* > otherCopee( copee );
    EXPECT_EQ( others.getCapacity(), 10 );
    EXPECT_EQ( others.getCount(), 10 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 10 );
    EXPECT_EQ( otherCopee.getCapacity(), 10 );
    EXPECT_EQ( otherCopee.getCount(), 10 );



    GenericIterator< TESTER* > otherIt( reinterpret_cast< TESTER** >( others.front() ) );
    GenericIterator< TESTER* > copeeIt( reinterpret_cast< TESTER** >( copee.front() ) );
    GenericIterator< TESTER* > otherCopeeIt( reinterpret_cast< TESTER** >( otherCopee.front() ) );

    
    EXPECT_NE( reinterpret_cast< int** >( others.front() ), reinterpret_cast< int** >( copee.front() ) );
    
    EXPECT_NE( reinterpret_cast< int* >( * ( others.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < others.getCount(); i++ )
    {
        EXPECT_EQ(  ( *otherIt ).total(), 15 );
    }

    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *copeeIt ).total(), 15 );
    }

    for ( int i = 0; i < otherCopee.getCount(); i++ )
    {
        EXPECT_EQ( ( *otherCopeeIt ).total(), 15 );
    }


    GenericContainer< int* > intz( 20, 66 );
    copee = intz;
    GenericIterator< int* > intzIt( reinterpret_cast< int** >( intz.front() ) );
    GenericIterator< int* > cops( reinterpret_cast< int** >( copee.front() ) );

    EXPECT_EQ( intz.getCapacity(), 20 );
    EXPECT_EQ( intz.getCount(), 20 );
    EXPECT_EQ( copee.getCapacity(), 20 );
    EXPECT_EQ( copee.getCount(), 20 );
    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *cops ), 66 );
    }

    for ( int i = 0; i < intz.getCount(); i++ )
    {
        EXPECT_EQ( ( *intzIt ), 66 );
    }
    
    

}
/****************************************************************************************/


TEST( GenericVoidPtr, CopyConstructorFromVoidPtrToTESTER )
{
    GenericContainer< void* > others( 10, TESTER( 5 ) );
    GenericContainer< TESTER* > copee( others );
    EXPECT_EQ( others.getCapacity(), 10 );
    EXPECT_EQ( others.getCount(), 10 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 10 );
    
    GenericIterator< TESTER* > otherIt( reinterpret_cast< TESTER** >( others.front() ) );
    GenericIterator< TESTER* > copeeIt( reinterpret_cast< TESTER** >( copee.front() ) );

    
    EXPECT_NE( reinterpret_cast< int** >( others.front() ), reinterpret_cast< int** >( copee.front() ) );
    EXPECT_NE( reinterpret_cast< int* >( * ( others.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < others.getCount(); i++ )
    {
        EXPECT_EQ(  ( *otherIt ).total(), 15 );
    }

    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *copeeIt ).total(), 15 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, CopyConstructorTESTERToTESTER )
{
    GenericContainer< TESTER* > others( 10, TESTER( 5 ) );
    GenericContainer< TESTER* > copee( others );
    EXPECT_EQ( others.getCapacity(), 10 );
    EXPECT_EQ( others.getCount(), 10 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 10 );
    
    GenericIterator< TESTER* > otherIt( reinterpret_cast< TESTER** >( others.front() ) );
    GenericIterator< TESTER* > copeeIt( reinterpret_cast< TESTER** >( copee.front() ) );

    
    EXPECT_NE( reinterpret_cast< int** >( others.front() ), reinterpret_cast< int** >( copee.front() ) );
    
    EXPECT_NE( reinterpret_cast< int* >( * ( others.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < others.getCount(); i++ )
    {
        EXPECT_EQ(  others[ i ].total(), 15 );
    }

    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *copeeIt ).total(), 15 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, CopyConstructorIntToVoid )
{
    GenericContainer< int* > others( 10, 17 );
    GenericContainer< void* > copee( others );
    EXPECT_EQ( others.getCapacity(), 10 );
    EXPECT_EQ( others.getCount(), 10 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 10 );
    
    GenericIterator< int* > otherIt( reinterpret_cast< int** >( others.front() ) );
    GenericIterator< int* > copeeIt( reinterpret_cast< int** >( copee.front() ) );

    
    EXPECT_NE( reinterpret_cast< int** >( others.front() ), reinterpret_cast< int** >( copee.front() ) );
    
    EXPECT_NE( reinterpret_cast< int* >( * ( others.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < others.getCount(); i++ )
    {
        EXPECT_EQ(  ( *otherIt ), 17 );
    }

    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *copeeIt ), 17 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, CopyConstructorVoidToInt )
{
    GenericContainer< void* > others( 10, 17 );
    GenericContainer< int* > copee( others );
    EXPECT_EQ( others.getCapacity(), 10 );
    EXPECT_EQ( others.getCount(), 10 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 10 );
    
    GenericIterator< int* > otherIt( reinterpret_cast< int** >( others.front() ) );
    GenericIterator< int* > copeeIt( reinterpret_cast< int** >( copee.front() ) );

    EXPECT_NE( reinterpret_cast< int** >( others.front() ), reinterpret_cast< int** >( copee.front() ) );
    
    EXPECT_NE( reinterpret_cast< int* >( * ( others.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < others.getCount(); i++ )
    {
        EXPECT_EQ(  ( *otherIt ), 17 );
    }

    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *copeeIt ), 17 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, CopyConstructorIntToInt )
{
    GenericContainer< int* > others( 10, 17 );
    GenericContainer< int* > copee( others );
    EXPECT_EQ( others.getCapacity(), 10 );
    EXPECT_EQ( others.getCount(), 10 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 10 );
    
    GenericIterator< int* > otherIt( reinterpret_cast< int** >( others.front() ) );
    GenericIterator< int* > copeeIt( reinterpret_cast< int** >( copee.front() ) );

    EXPECT_NE( reinterpret_cast< int** >( others.front() ), reinterpret_cast< int** >( copee.front() ) );
    
    EXPECT_NE( reinterpret_cast< int* >( * ( others.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < others.getCount(); i++ )
    {
        EXPECT_EQ(  others[ i ], 17 );
    }

    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *copeeIt ), 17 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, Swapper )
{
    GenericContainer< void* > movee( 10, TESTER( 5 ) );
    GenericContainer< void* > mover;
    mover.swap( movee );

    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCount(), 10 );
    EXPECT_EQ( mover.getCapacity(), 10 );
    GenericIterator< TESTER* > testTester( mover.begin() );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ).total(), 15 );
    }
    
    
}
/****************************************************************************************/

TEST( GenericVoidPtr, MoveConstructor )
{
    GenericContainer< void* > movee( 10, TESTER( 5 ) );
    GenericContainer< void* > mover( std::move( movee ) );

    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCapacity(), 10 );
    EXPECT_EQ( mover.getCount(), 10 );

    GenericIterator< TESTER* > testTester( mover.begin() );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ).total(), 15 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, MoveConstructorFromVoidToTESTER )
{
    GenericContainer< void* > movee( 10, TESTER( 5 ) );
    GenericContainer< TESTER* > mover( std::move( movee ) );

    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCapacity(), 10 );
    EXPECT_EQ( mover.getCount(), 10 );

    GenericIterator< TESTER* > testTester( mover.begin() );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ).total(), 15 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, MoveConstructorFromTESTERToTESTER )
{
    GenericContainer< TESTER* > movee( 10, TESTER( 5 ) );
    GenericContainer< TESTER* > mover( std::move( movee ) );

    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCapacity(), 10 );
    EXPECT_EQ( mover.getCount(), 10 );

    GenericIterator< TESTER* > testTester( reinterpret_cast< TESTER** >( mover.front() ) );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ).total(), 15 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, MoveConstructorFromTESTERToVoid )
{
    GenericContainer< TESTER* > movee( 10, TESTER( 5 ) );
    GenericContainer< void* > mover( std::move( movee ) );


    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCapacity(), 10 );
    EXPECT_EQ( mover.getCount(), 10 );


    GenericIterator< TESTER* > testTester( reinterpret_cast< TESTER** >( mover.front() ) );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ).total(), 15 );
    }
}
/****************************************************************************************/

/****************************************************************************************/

TEST( GenericVoidPtr, MoveConstructorFromVoidToInt )
{
    GenericContainer< void* > movee( 10, 22 );
    GenericContainer< int* > mover( std::move( movee ) );

    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCapacity(), 10 );
    EXPECT_EQ( mover.getCount(), 10 );

    GenericIterator< int* > testTester( mover.begin() );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ), 22 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, MoveConstructorFromIntToInt )
{
    GenericContainer< int* > movee( 10, 42 );
    GenericContainer< int* > mover( std::move( movee ) );

    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCapacity(), 10 );
    EXPECT_EQ( mover.getCount(), 10 );

    GenericIterator< int* > testTester( reinterpret_cast< int** >( mover.front() ) );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ), 42 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, MoveConstructorFromIntToVoid )
{
    GenericContainer< int* > movee( 10, 200 );
    GenericContainer< void* > mover( std::move( movee ) );


    EXPECT_EQ( movee.getCapacity(), 0 );
    EXPECT_EQ( movee.getCount(), 0 );
    EXPECT_EQ( movee.front(), nullptr );

    EXPECT_EQ( mover.getCapacity(), 10 );
    EXPECT_EQ( mover.getCount(), 10 );


    GenericIterator< int* > testTester( reinterpret_cast< int** >( mover.front() ) );

    for ( int i = 0; i < mover.getCount(); i++ )
    {
        EXPECT_EQ( ( *testTester ), 200 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, PushBack )
{
    GenericContainer< void* > first( 10, TESTER( 5 ) );
    first.pushBack( new TESTER( 2 ) );

    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 11 );
    EXPECT_EQ( ( reinterpret_cast< TESTER* >( first[ 10 ] ) )->total(), 6 );

    first.pushBack( new TESTER( 40 ) );
    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 12 );
    EXPECT_EQ( ( reinterpret_cast< TESTER* >( first[ 11 ] ) )->total(), 120 );


    GenericContainer< void* > second;
    second.pushBack( new TESTER( 9 ) );
    EXPECT_EQ( second.getCapacity(), 10 );
    EXPECT_EQ( second.getCount(), 1 );
    EXPECT_EQ( ( reinterpret_cast< TESTER* >( second[ 0 ] ) )->total(), 27 );
}
/****************************************************************************************/

TEST( GenericVoidPtr, PushBackTESTER )
{
    GenericContainer< TESTER* > first( 10, TESTER( 5 ) );
    first.pushBack( new TESTER( 2 ) );

    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 11 );
    EXPECT_EQ( ( first[ 10 ] ).total(), 6 );

    first.pushBack( new TESTER( 40 ) );
    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 12 );
    EXPECT_EQ( ( first[ 11 ] ).total(), 120 );


    GenericContainer< int* > second;
    for ( int i = 0; i < 5; i++ )
    {
        second.pushBack( new int( 45 ) );
        EXPECT_EQ( second[ i ], 45 );
    }
    EXPECT_EQ( second.getCount(), 5 );
    EXPECT_EQ( second.getCapacity(), 10 );
    
}
/****************************************************************************************/

TEST( GenericVoidPtr, PushBackInt )
{
    GenericContainer< int* > first( 10, 222 );
    first.pushBack( new int( 40 ) );

    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 11 );
    EXPECT_EQ( ( first[ 10 ] ), 40 );

    first.pushBack( new int( 777 ) );
    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 12 );
    EXPECT_EQ( ( first[ 11 ] ), 777 );


    GenericContainer< int* > second;
    second.pushBack( new int( 123 ) );
    EXPECT_EQ( second.getCapacity(), 10 );
    EXPECT_EQ( second.getCount(), 1 );
    EXPECT_EQ( ( second[ 0 ] ), 123 );
}
/****************************************************************************************/

TEST( GenericVoidPtr, PushBackAllTypes )
{
    GenericContainer< void* > first( 10, TESTER( 5 ) );
    first.pushBack( new TESTER( 2 ) );

    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 11 );
    EXPECT_EQ( ( reinterpret_cast< TESTER* >( first[ 10 ] ) )->total(), 6 );

    first.pushBack( new int( 200) );
    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 12 );
    EXPECT_EQ( *( reinterpret_cast< int* >( first[ 11 ] ) ), 200 );


    GenericContainer< void* > second;
    second.pushBack( new double( 40.0 ) );
    EXPECT_EQ( second.getCapacity(), 10 );
    EXPECT_EQ( second.getCount(), 1 );
    EXPECT_EQ( *( reinterpret_cast< double* >( second[ 0 ] ) ), 40.0 );
}
/****************************************************************************************/

TEST( GenericVoidPtr, PopBack )
{
    GenericContainer< void* > first( 10, TESTER( 5 ) );
    first.popBack();

    EXPECT_EQ( first.getCapacity(), 10 );
    EXPECT_EQ( first.getCount(), 9 );

    for ( int i = 0; i < first.getCount(); i++ )
    {
        EXPECT_EQ( ( reinterpret_cast< TESTER* >( first[ i ] ) )->total(), 15 );
    }
}
// /****************************************************************************************/

TEST( GenericVoidPtr, Reserve )
{
    GenericContainer< void* > first( 10, TESTER( 5 ) );
    first.reserve( 20 );
    first.pushBack( new TESTER( 5 ) );

    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 11 );



    for ( int i = 0; i < first.getCount(); i++ )
    {
        EXPECT_EQ( ( reinterpret_cast< TESTER* >( first[ i ] ) )->total(), 15 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, Shrink )
{
    GenericContainer< void* > first( 10, TESTER( 5 ) );
    first.reserve( 20 );
    first.pushBack( new TESTER( 5 ) );

    EXPECT_EQ( first.getCapacity(), 20 );
    EXPECT_EQ( first.getCount(), 11 );

    first.shrink_to_fit();

    EXPECT_EQ( first.getCapacity(), 11 );
    EXPECT_EQ( first.getCount(), 11 );



    for ( int i = 0; i < first.getCount(); i++ )
    {
        EXPECT_EQ( ( reinterpret_cast< TESTER* >( first[ i ] ) )->total(), 15 );
    }

}
/****************************************************************************************/

TEST( EXTRASTUFF, VariadicRetrieveNumber )
{
    EXPECT_EQ( 7, retrieveNumberDependingOnParameterCount( 1, 2, 4, 5, 6, 8, 9) );
    EXPECT_EQ( 2, retrieveNumberDependingOnParameterCount( 2, 222 ) );
}
/****************************************************************************************/

TEST( EXTRASTUFF, VariadicStructure )
{
    VariadicStruct< int, double, std::string, char > myStruct( 2, 4.0, "Paradise-city", 'c' );

    int someVal = myStruct.value;
    EXPECT_EQ( 4, someVal );
}
/****************************************************************************************/

// TEST( EXTRASTUFF, BitRollingStuff )
// {
//     uint32_t regist = Binary<1111110>::value;

//     uint32_t someReadVal = read( &regist, 1, 4 );
//     uint32_t someOtherReadVal = read( &regist, 2, 5 );
//     uint32_t tester = 14;
//     uint32_t otherTester = 32;

//     EXPECT_EQ( someReadVal, tester );
//     EXPECT_EQ( someOtherReadVal, otherTester );
// }
// /****************************************************************************************/

TEST( EXTRASTUFF, IfTHENElse )
{
    using myType = IfThenElse< false, char, int >::type;
    bool vals = std::is_same< int, myType >::value;
    EXPECT_EQ( vals, true );

    using myOtherType = IfThenElse< true, char, int >::type;
    bool value = std::is_same< char, myOtherType >::value;
    EXPECT_EQ( value, true );
}
/****************************************************************************************/

TEST( EXTRASTUFF, STDMAX )
{
    int a = 200;
    int b = 400;
    int c = 800;

    int p = 0;
    int q = 0;

    EXPECT_EQ( ( dummy::retrieveHighestNumber( a, b ) ), 400 );
    EXPECT_EQ( ( dummy::retrieveHighestNumber( c, b ) ), 800 );
    EXPECT_EQ( ( dummy::retrieveHighestNumber( p, q ) ),   0 );
}
/****************************************************************************************/

TEST( DRUGS, ItemCopying )
{
    GenericContainer< Item* > myBag;

    myBag.pushBack( new DrugLord::Weed( 100 ) );
    myBag.pushBack( new DrugLord::Cocaine( 100 ) );
    myBag.pushBack( new DrugLord::Heroine( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );

    GenericContainer< Item* > otherBag( myBag );

    // for ( int i = 0; i < myBag.getCount(); i++ )
    // {
    //     otherBag[ i ].printType();
    //     std::cout << "\n";
    // }
}
/****************************************************************************************/

TEST( GenericVoidPtr, CopyConstructorFromItemToVoidToItem )
{
    GenericContainer< Item* > bag;
    bag.pushBack( new DrugLord::Weed( 100 ) );
    bag.pushBack( new DrugLord::Cocaine( 100 ) );
    bag.pushBack( new DrugLord::Heroine( 100 ) );
    bag.pushBack( new DrugLord::LSD( 100 ) );
    bag.pushBack( new DrugLord::Weed( 100 ) );
    bag.pushBack( new DrugLord::Cocaine( 100 ) );
    bag.pushBack( new DrugLord::Heroine( 100 ) );
    bag.pushBack( new DrugLord::LSD( 100 ) );


    GenericContainer< void* > copee( bag );
    GenericContainer< Item* > otherCopee( copee );
    EXPECT_EQ( bag.getCapacity(), 10 );
    EXPECT_EQ( bag.getCount(), 8 );
    EXPECT_EQ( copee.getCapacity(), 10 );
    EXPECT_EQ( copee.getCount(), 8 );
    EXPECT_EQ( otherCopee.getCapacity(), 10 );
    EXPECT_EQ( otherCopee.getCount(), 8 );



    GenericIterator< Item* > otherIt( reinterpret_cast< Item** >( bag.front() ) );
    GenericIterator< Item* > copeeIt( reinterpret_cast< Item** >( copee.front() ) );
    GenericIterator< Item* > otherCopeeIt( reinterpret_cast< Item** >( otherCopee.front() ) );

    
    EXPECT_NE( reinterpret_cast< int** >( bag.front() ), reinterpret_cast< int** >( copee.front() ) );
    
    EXPECT_NE( reinterpret_cast< int* >( * ( bag.front() ) ), reinterpret_cast< int* >( * ( copee.front() ) ) );

    for ( int i = 0; i < bag.getCount(); i++ )
    {
        EXPECT_EQ( ( otherIt[ i ].getQuantity() ), 100 );
    }

    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( copeeIt[ i ].getQuantity() ), 100 );
    }

    for ( int i = 0; i < otherCopee.getCount(); i++ )
    {
        EXPECT_EQ( ( otherCopeeIt[ i ].getQuantity() ), 100 );
    }


    GenericContainer< int* > intz( 20, 66 );
    copee = intz;
    GenericIterator< int* > intzIt( reinterpret_cast< int** >( intz.front() ) );
    GenericIterator< int* > cops( reinterpret_cast< int** >( copee.front() ) );

    EXPECT_EQ( intz.getCapacity(), 20 );
    EXPECT_EQ( intz.getCount(), 20 );
    EXPECT_EQ( copee.getCapacity(), 20 );
    EXPECT_EQ( copee.getCount(), 20 );
    for ( int i = 0; i < copee.getCount(); i++ )
    {
        EXPECT_EQ( ( *cops ), 66 );
    }

    for ( int i = 0; i < intz.getCount(); i++ )
    {
        EXPECT_EQ( ( *intzIt ), 66 );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, FromItemToInt )
{
    GenericContainer< Item* > myBag;

    myBag.pushBack( new DrugLord::Weed( 100 ) );
    myBag.pushBack( new DrugLord::Cocaine( 100 ) );
    myBag.pushBack( new DrugLord::Heroine( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );

    GenericContainer< int* > otherBag( myBag );
    GenericIterator< Item* > itemIt( reinterpret_cast< Item** >( otherBag.front() ) );

    // for ( int i = 0; i < otherBag.getCount(); i++ )
    // {
    //     itemIt[ i ].printType();
    //     std::cout << "\n";
    // }
}
/****************************************************************************************/

TEST( GenericVoidPtr, FromIntToTESTER )
{
    GenericContainer< TESTER* > myBag;

    for ( int i = 0; i < 13; i++ )
    {
        myBag.pushBack( new TESTER( i ) );
    }
    

    GenericContainer< int* > otherBag( myBag );
    GenericIterator< TESTER* > itemIt( reinterpret_cast< TESTER** >( otherBag.front() ) );

    for ( int i = 0; i < otherBag.getCount(); i++ )
    {
        EXPECT_EQ( itemIt[ i ].total(), ( i * 3 ) );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, FromIntToTESTERMoving )
{
    GenericContainer< TESTER* > myBag;

    for ( int i = 0; i < 13; i++ )
    {
        myBag.pushBack( new TESTER( i ) );
    }
    

    GenericContainer< int* > otherBag( std::move( myBag ) );
    GenericIterator< TESTER* > itemIt( reinterpret_cast< TESTER** >( otherBag.front() ) );

    for ( int i = 0; i < otherBag.getCount(); i++ )
    {
        EXPECT_EQ( itemIt[ i ].total(), ( i * 3 ) );
    }
}
/****************************************************************************************/

TEST( GenericVoidPtr, FromItemToIntMove )
{
    GenericContainer< Item* > myBag;

    myBag.pushBack( new DrugLord::Weed( 100 ) );
    myBag.pushBack( new DrugLord::Cocaine( 100 ) );
    myBag.pushBack( new DrugLord::Heroine( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );
    myBag.pushBack( new DrugLord::LSD( 100 ) );

    GenericContainer< int* > otherBag( std::move( myBag ) );

    EXPECT_EQ( myBag.getCount(), 0 );
    EXPECT_EQ( myBag.getCapacity(), 0 );
    EXPECT_EQ( myBag.front(), nullptr );

    GenericIterator< Item* > itemIt( reinterpret_cast< Item** >( otherBag.front() ) );

    // for ( int i = 0; i < otherBag.getCount(); i++ )
    // {
    //     itemIt[ i ].printType();
    //     std::cout << "\n";
    // }
}
/****************************************************************************************/

TEST( GenericVoidPtr, FunctionalityToShowInRapport )
{
    GenericContainer< Item* > backPack; // <--------------------------------------------------------------------- ( 1 )

    backPack.pushBack( new DrugLord::Weed( 100 ) ); // <--------------------------------------------------------- ( 2 )
    backPack.pushBack( new DrugLord::LSD( 1.0_kg ) );

    GenericContainer< void* > backPackCopy( backPack ); // <----------------------------------------------------- ( 3 )

    GenericContainer< void* > backPackMover( std::move( backPack ) );// <---------------------------------------- ( 4 )

    GenericIterator< Item* > itemPtrIterator(  reinterpret_cast< Item** >( backPackCopy.front()  ) ); // <------- ( 5 )
    GenericIterator< Item* > itemPtrIterator1( reinterpret_cast< Item** >( backPackMover.front() ) );

    itemPtrIterator[ 0 ].printType(); std::cout << "\n"; itemPtrIterator1[ 0 ].printType(); std::cout << "\n"; // ( 6 )

    GenericContainer< int* > intBackPack( 10, 222 ); // <-------------------------------------------------------- ( 7 )

    backPackMover = std::move( intBackPack ); // <--------------------------------------------------------------- ( 8 )

    GenericIterator< int* > intPtrIterator( reinterpret_cast< int** >( backPackMover.front() ) ); // <----------- ( 9 )

    EXPECT_EQ( backPack.getCapacity(), 0 ); EXPECT_EQ( intBackPack.getCapacity(), 0 ); // <---------------------- ( 10 )

    EXPECT_EQ( intPtrIterator[ 0 ], 222 ); // <------------------------------------------------------------------ ( 11 )
    
}
/****************************************************************************************/

TEST( GenericVoidPtr, OtherSnippetInRapport )
{
    GenericContainer< int* > intContainer( 15, 123 );  // <------------------------------- ( 1 )

    GenericContainer< void* > voidContainer( intContainer ); // <------------------------- ( 2 )

    GenericContainer< int* > copyingContainer( voidContainer ); // <---------------------- ( 3 )

    EXPECT_EQ( copyingContainer.getCount(), intContainer.getCount() ); // <--------------- ( 4 )
    EXPECT_EQ( copyingContainer.getCapacity(), intContainer.getCapacity() ); // <--------- ( 5 )

    for ( int i = 0; i < intContainer.getCount(); i++ )
    {
        EXPECT_EQ( copyingContainer[ i ], intContainer[ i ] ); // <----------------------- ( 6 )
    }
}
/****************************************************************************************/

#endif //__GENERIC_CONTAINER_VOID_PTR_TEST_HPP__