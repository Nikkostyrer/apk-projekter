#ifndef __GENERICCONTAINERTEST_HPP__
#define __GENERICCONTAINERTEST_HPP__

#include "GenericContainer.hpp"
#include "algorithms.hpp"


/****************************************************************************************/
/*                                   Constructor tests.                                 */
/****************************************************************************************/
TEST( genericContainer, DefaultConstructor )
{
    // Test of default constructor.
    GenericContainer< int > deffault;
    ASSERT_EQ( 0, deffault.getCapacity() );
    ASSERT_EQ( 0, deffault.getCount() );
    ASSERT_EQ( nullptr, &(deffault.front() ) );
}
/****************************************************************************************/

TEST( genericContainer, DefaultConstructorAndPushback )
{
    // Test of default constructor.
    GenericContainer< int > deffault;
    ASSERT_EQ( 0, deffault.getCapacity() );
    ASSERT_EQ( 0, deffault.getCount() );
    ASSERT_EQ( nullptr, &(deffault.front() ) );

    deffault.pushBack( 1 );
    deffault.pushBack( 2 );
    deffault.pushBack( 3 );
    //deffault.pushBack( 4 );
    //deffault.pushBack( 5 );
    //deffault.pushBack( 6 );
    //deffault.pushBack( 7 );

    ASSERT_EQ( deffault.getCapacity(), 10 );
    ASSERT_EQ( deffault.getCount(), 3  );

    for ( int i = 0; i < deffault.getCount(); i++ )
    {
        ASSERT_EQ( deffault[ i ], ( i + 1 ) );
    }
}
/****************************************************************************************/

TEST( genericContainer, CapacityConstructor )
{
    // Test of capacity constructor.
    GenericContainer< int > capper( 15 );
    ASSERT_EQ( 15, capper.getCapacity() );
    for ( int i = 0; i < 15; i++ )
    {
        //capper[ i ] = i;
    }
    for ( int i = 0; i < 15; i++ )
    {
        //ASSERT_EQ( i, capper[ i ] );
    }
    ASSERT_EQ( 0, capper.getCount() );
}
/****************************************************************************************/

TEST( genericContainer, FillerConstructor )
{
    int someVal = 666;
    // Test of constructor that fills the full capacity.
    GenericContainer< int > filler( 20, someVal );
    for ( int i = 0; i < 20; i++ )
    {
        ASSERT_EQ( 666, filler[ i ] );
    }
    ASSERT_EQ( 20, filler.getCount() );
    ASSERT_EQ( 20, filler.getCapacity() );
}
/****************************************************************************************/

TEST( genericContainer, PredefinedDataConstructor )
{
    // Test of constructor with predefined data.

    // Predefined data.
    int* someVals = new int[ 11 ];

    // Filling data with the index value.
    for ( int i = 0; i < 11; i++ )
    {
        someVals[ i ] = i;
    }

    // Creating the Container to contain predefined data.
    GenericContainer< int > predef( someVals, 11, 11 );

    // Assert that the data in the container contains index.
    for ( int i = 0; i < 11; i++ )
    {
        ASSERT_EQ( i, predef[ i ] );
    }

    // Assert that capacity is given from the constructor.
    ASSERT_EQ( 11, predef.getCapacity() );

    // Assert that count is at max capacity.
    ASSERT_EQ( 11, predef.getCount() );
}
/****************************************************************************************/

TEST( genericContainer, CopyConstructor )
{
    int someVal = 666;

    // Creates a container with 20 slots, all containing 666.
    GenericContainer< int > filler( 20, someVal );

    // Copy from that container.
    GenericContainer< int > copied( filler );

    // Test that capacity and count equals of the one copied from.
    ASSERT_EQ( filler.getCapacity(), copied.getCapacity() );
    ASSERT_EQ( filler.getCount(), copied.getCount() );

    // Test that all values are actually the same after copying.
    for ( int i = 0; i < filler.getCapacity(); i++)
    {
        filler[ i ] = filler[ i ] + 1;
        EXPECT_EQ( filler[ i ], copied[ i ] + 1 );
    }
}
/****************************************************************************************/

TEST( genericContainer, CopyFromOtherTypeConstructor )
{
    if( std::is_convertible< int, double >::value )
    {
        int someVal = 666;

        // Creates a container with 20 slots, all containing 666.
        GenericContainer< int > filler( 20, someVal );

        // Copy from that container.
        GenericContainer< double > copied( filler );

        // Test that capacity and count equals of the one copied from.
        ASSERT_EQ( filler.getCapacity(), copied.getCapacity() );
        ASSERT_EQ( filler.getCount(), copied.getCount() );

        // Test that all values are actually the same after copying.
        for ( int i = 0; i < filler.getCapacity(); i++)
        {
            EXPECT_EQ( filler[ i ], copied[ i ] );
        }
    }
}
/****************************************************************************************/
//TODO::
TEST( genericContainer, MoveConstructor )
{
    int someVal = 666;

    // Creates a container with 20 slots, all containing 666.
    GenericContainer< int > filler( 20, someVal );

    // Copy from that container.
    GenericContainer< int > mover( std::move( filler ) );

    // Test that the one being moved from has 0 in cap and count.
    ASSERT_EQ( filler.getCapacity(), 0 );
    ASSERT_EQ( filler.getCount(), 0 );

    // Test that mover has 20 in cap and count now.
    ASSERT_EQ( mover.getCapacity(), 20 );
    ASSERT_EQ( mover.getCount(), 20 );

    // Test that all values are actually the same after copying.
    for ( int i = 0; i < mover.getCapacity(); i++)
    {
        ASSERT_EQ( mover[ i ], 666 );
    }
}

/****************************************************************************************/
/*                                   Operator tests.                                    */
/****************************************************************************************/
TEST( genericContainer, pushBack )
{
    int someVal = 666;

    // Creates a container with 20 slots, all containing 666.
    GenericContainer< int > filler( 20, someVal );

    // Push something back in the filer.
    filler.pushBack( 20 );

    // Expect filler to have double capacity now.
    ASSERT_EQ( 40, filler.getCapacity() );

    // Expect filler's count to be one-upped.
    EXPECT_EQ( 21, filler.getCount() );

    // Expect that 666 is still in slot 19, the 20th slot.
    EXPECT_EQ( 666, filler[ 19 ] );

    // Afterwards is the 20 that is pushed back.
    EXPECT_EQ( 20, filler[ 20 ] );

    // Try one more time.
    filler.pushBack( 200 );

    // Expect filler to have double capacity now.
    ASSERT_EQ( 40, filler.getCapacity() );

    // Expect filler's count to be one-upped.
    EXPECT_EQ( 22, filler.getCount() );

    // Expect that 666 is still in slot 19, the 20th slot.
    EXPECT_EQ( 20, filler[ 20 ] );

    // Afterwards is the 20 that is pushed back.
    EXPECT_EQ( 200, filler[ 21 ] );
}
/****************************************************************************************/

TEST( genericContainer, Swapper )
{
    int someVal = 666;
    int someOtherVal = 222;

    // Creates a container with 20 slots, all containing 666.
    GenericContainer< int > filler( 20, someVal );

    // Creates another container to swap with.
    GenericContainer< int > otherFiller( 10, someOtherVal );

    // ASSERT that they are allocated normally.
    ASSERT_EQ( filler.getCount(), otherFiller.getCount() + 10 );
    ASSERT_EQ( filler.getCapacity(), otherFiller.getCapacity() + 10 );
    for ( int i = 0; i < filler.getCount(); i++ )
    {
        ASSERT_EQ( filler[ i ], 666 );
        if ( i < otherFiller.getCount() )
        {
            ASSERT_EQ( otherFiller[ i ], 222 );
        }
    }

    filler.swap( otherFiller );

    EXPECT_EQ( filler.getCount(), otherFiller.getCount() - 10 );
    EXPECT_EQ( filler.getCapacity(), otherFiller.getCapacity() - 10 );
    for ( int i = 0; i < otherFiller.getCount(); i++ )
    {
        EXPECT_EQ( otherFiller[ i ], 666 );
        if ( i < filler.getCount() )
        {
            EXPECT_EQ( filler[ i ], 222 );
        }
    }
}
/****************************************************************************************/

TEST( genericContainer, popper )
{
    int someVal = 666;

    // Creates a container with 20 slots, all containing 666.
    GenericContainer< int > filler( 20, someVal );

    filler.popBack();

    EXPECT_EQ( filler.getCapacity(), 20 );
    EXPECT_EQ( filler.getCount(), 19 );
}
/****************************************************************************************/

TEST( genericContainer, ShrinkToFit )
{
    int someVal = 666;

    // Creates a container with 20 slots, all containing 666.
    GenericContainer< int > filler( 20, someVal );

    filler.popBack();

    EXPECT_EQ( filler.getCapacity(), 20 );
    EXPECT_EQ( filler.getCount(), 19 );

    filler.popBack();

    EXPECT_EQ( filler.getCapacity(), 20 );
    EXPECT_EQ( filler.getCount(), 18 );
}
/****************************************************************************************/

TEST( genericContainer, Reserve )
{
    int someVal = 666;

    // Creates a container with 20 slots, all containing 666.
    GenericContainer< int > filler( 20, someVal );

    EXPECT_EQ( filler.getCapacity(), 20 );
    EXPECT_EQ( filler.getCount(), 20 );

    filler.reserve( 10 );
}
/****************************************************************************************/

// TEST( algorithm, ActualCopying )
// {
//     // // Check that checkCopyable on int is true.
//     // EXPECT_EQ( ( std::is_same< checkCopyable< int >::checked, isTrivial >::value ), true );

//     // // Check that checkCopyable on int* is false.
//     // EXPECT_EQ( ( std::is_same< checkCopyable< int* >::checked, isTrivial >::value ), true );

//     //EXPECT_EQ( std::is_trivially_copyable< int* >::value, false);

//     // // Check what tester is.
//     // EXPECT_EQ( ( std::is_same< checkCopyable< TESTER* >::checked, notTrivial >::value ), true );

//     EXPECT_EQ( ( std::is_same< checkCopyable< TESTER >::checked, isTrivial >::value ), false );

//     // Create integer array.
//     int someArray[ 10 ];
//     std::fill( someArray, someArray + 10, 20 );

//     // Copy that integer array to another.
//     int someOtherArray[ 10 ];
//     std::fill( someOtherArray, someOtherArray + 10, 5 );
//     someOtherArray[0] = 2;

//     // Try copying.
//     algorithms::copyUsingTag< int >( someArray, someArray + 10, someOtherArray );
//     for ( int i = 0; i < 10; i++ )
//     {
//         EXPECT_EQ( someOtherArray[ i ], 20 );
//     }

//     int testz = 25;


//     TESTER* first = new TESTER( 10 );
//     TESTER* second = new TESTER( 200 );
//     TESTER* thirt = new TESTER( 35 );


//     TESTER* otherFirst = new TESTER( 90 );
//     TESTER* otherSecond = new TESTER( 75 );
//     TESTER* otherThird = new TESTER( 432 );


//     TESTER** firstRealPtrPtr = new TESTER*[3];
//     TESTER** secondRealPtrPtr = new TESTER*[3];

//     firstRealPtrPtr[ 0 ] = first;
//     firstRealPtrPtr[ 1 ] = second;
//     firstRealPtrPtr[ 2 ] = thirt;

//     secondRealPtrPtr[ 0 ] = otherFirst;
//     secondRealPtrPtr[ 1 ] = otherSecond;
//     secondRealPtrPtr[ 2 ] = otherThird;
    
//     algorithms::copySmart( firstRealPtrPtr, firstRealPtrPtr + 3, secondRealPtrPtr );

//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( firstRealPtrPtr[ 0 ] ) ).data[ i ], 10 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( firstRealPtrPtr[ 1 ] ) ).data[ i ], 200 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( firstRealPtrPtr[ 2 ] ) ).data[ i ], 35 );
//     }
    

//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( secondRealPtrPtr[ 0 ] ) ).data[ i ], 10 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( secondRealPtrPtr[ 1 ] ) ).data[ i ], 200 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( secondRealPtrPtr[ 2 ] ) ).data[ i ], 35 );
//     }
    


//     bool p = isPointer< int* >::value;
//     bool q = isPointer< int >::value;
//     EXPECT_EQ( p, true );
//     EXPECT_EQ( q, false );

//     bool t = isPointerToPointer< int >::value;
//     using ttype = isPointerToPointer< int >::type;
//     bool tttypeCom = std::is_same< ttype, notPointerType >::value;

//     bool s = isPointerToPointer< int** >::value;
//     using stype = isPointerToPointer< int** >::type;
//     bool stypeCom = std::is_same< stype, pointerType >::value;

//     EXPECT_EQ( t, false );
//     EXPECT_EQ( s, true );

//     bool k = isPointerToPointer< int* >::value;
//     using ktype = isPointerToPointer< int* >::type;
//     bool ktypeCom = std::is_same< ktype, notPointerType>::value;
//     EXPECT_EQ( k, false );


//     // Check if types are assumed correctly.
//     EXPECT_EQ( true, tttypeCom );
//     EXPECT_EQ( true, stypeCom );
//     EXPECT_EQ( true, ktypeCom );



//     bool dPtrPtr = isPointerToPointer< double** >::value;
//     bool dPtr = isPointerToPointer< double* >::value;
//     bool d = isPointerToPointer< double >::value;

//     void** myVoidPtrPtr = nullptr;

//     //std::cout << std::is_same< isPointerToPointer< void* >::type, pointerType >::value << "\n";

//     //GenericIterator myVoidPtrPtrItter( myVoidPtrPtr );

//     void** otherVoidPtrPTr = nullptr;

//     algorithms::copySmart( myVoidPtrPtr, myVoidPtrPtr, otherVoidPtrPTr );

// }
// /****************************************************************************************/


// TEST( algorithm, VoidPtrPtrTest )
// {
//     TESTER* first = new TESTER( 10 );
//     TESTER* second = new TESTER( 200 );
//     TESTER* thirt = new TESTER( 35 );


//     TESTER* otherFirst = new TESTER( 90 );
//     TESTER* otherSecond = new TESTER( 75 );
//     TESTER* otherThird = new TESTER( 432 );


//     TESTER** firstRealPtrPtr = new TESTER*[3];
//     TESTER** secondRealPtrPtr = new TESTER*[3];

//     firstRealPtrPtr[ 0 ] = first;
//     firstRealPtrPtr[ 1 ] = second;
//     firstRealPtrPtr[ 2 ] = thirt;

//     secondRealPtrPtr[ 0 ] = otherFirst;
//     secondRealPtrPtr[ 1 ] = otherSecond;
//     secondRealPtrPtr[ 2 ] = otherThird;

//     void** firstVoidPtrPtr = reinterpret_cast< void** >( firstRealPtrPtr );
//     void** secondVoidPtrPtr = reinterpret_cast< void** >( secondRealPtrPtr );
    
//     algorithms::copySmart( firstRealPtrPtr, firstRealPtrPtr + 3, secondRealPtrPtr );

//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( firstRealPtrPtr[ 0 ] ) ).data[ i ], 10 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( firstRealPtrPtr[ 1 ] ) ).data[ i ], 200 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( firstRealPtrPtr[ 2 ] ) ).data[ i ], 35 );
//     }
    

//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( secondRealPtrPtr[ 0 ] ) ).data[ i ], 10 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( secondRealPtrPtr[ 1 ] ) ).data[ i ], 200 );
//     }
//     for (int i = 0; i < 3; i++)
//     {
//         EXPECT_EQ( (*( secondRealPtrPtr[ 2 ] ) ).data[ i ], 35 );
//     }
// }

#endif //__GENERICCONTAINERTEST_HPP__