#ifndef __GENERIC_CONTAINER_HPP__
#define __GENERIC_CONTAINER_HPP__

#include "BaseContainer.hpp"
#include "algorithms.hpp"
#include <limits>
#include "GenericIterator.hpp"
#include <iostream>

/**
 * The generic container type.
 * Other containers can inherit or specialize this.
 */
template< typename T >
class GenericContainer : public BaseContainer
{
    /****************************************************************************************/
    /*                                   Class typedefs.                                    */
    /****************************************************************************************/
    typedef GenericIterator< T > iterator;
    typedef const GenericIterator< T > const_iterator;

    /****************************************************************************************/
    /*                                   Class members.                                     */
    /****************************************************************************************/
    // Pointer for storing data.
    T* data = nullptr;

    // The number of items in the container currently.
    int count;

    // The space in the container.
    int capacity;

public:
    /****************************************************************************************/
    /*                                   Class constructors.                                */
    /****************************************************************************************/
    /**
     * Creates an empty container with 1 capacity.
     * Basic default constructor.
     */
    GenericContainer() 
    {
        capacity = 0.0; // <-- Implicit conversion.
        count = NULL;   // <-- Implicit conversion.
        data = nullptr;
    }
    /****************************************************************************************/

    /**
     * Creates an empty container with predefined capacity.
     * Delegating constructor.
     * 
     * @param[in]  capacityIn               The capacity of the container.
     */
    GenericContainer( int capacityIn ) : GenericContainer()
    {
        try
        {
            data = new T[ capacityIn ];
            // for (int i = 0; i < capacityIn; i++ )
            // {
            //     data[ i ] = NULL;
            // }
            
            capacity = capacityIn;
        }
        catch( ... )
        {
            delete data;
            capacity = 0;
            throw;
        }
        
    }
    /****************************************************************************************/

    /**
     * Creates a container at defined capacity full of equal objects.
     * Explicit constructor.
     * 
     * @param[in] capacityIn                The capacity of the container.
     * @param[in] toFill                    The object to copy into all slots.
     */
    GenericContainer( int capacityIn, T& toFill ) : GenericContainer( capacityIn )
    {
        count = capacity = capacityIn;
        for ( int i = 0; i < capacityIn; i++ )
        {
            data[ i ] = toFill;
        }
    }
    /****************************************************************************************/

    /**
     * Creates a container with predefined data, datasize and capacity.
     * 
     * @param[in] dataIn                    The pre-created data.
     * @param[in] countIn                   The number of object in the container.
     * @param[in] capacityIn                The capacity of the container.
     */
    GenericContainer( T* dataIn, int countIn, int capacityIn ) : data( dataIn ), count( countIn ), capacity( capacityIn)
    {}
    /****************************************************************************************/

    /**
     * Creates a container similar to the one supplied.
     * 
     * @param[in] copyFrom                  The container to COPY from.
     */       
    template< typename U >          
    GenericContainer( const GenericContainer< U >& copyFrom ) : GenericContainer()
    {
        GenericContainer< T > temp;

        temp = copyFrom;

        swap( temp );
    }
    /****************************************************************************************/

    /**
     * Creates a container similar to the one supplied.
     * 
     * @param[in] copyFrom                  The container to COPY from.
     */                
    GenericContainer( const GenericContainer< T >& copyFrom ) : GenericContainer()
    {
        ( *this ) = copyFrom;
    }
    /****************************************************************************************/

    
    /**
     * Moves everything from applied container here.
     * 
     * @param[in]                           The container to move data from.
     */
    GenericContainer( GenericContainer< T >&& moveFrom ) : GenericContainer()
    {
        // Creates temp object with the capacity of the one moving from.
        GenericContainer< T > temp( moveFrom.capacity );

        // This creates a pointer to data, that needs to be deleted aswell. ^^^^
        // We set data to look at that, so that temp deletes it later.
        data = temp.data;

        // Get temp to point to the data of the movable object.
        temp.data = moveFrom.data;

        // Retrieve that count aswell.
        temp.count = moveFrom.count;

        // Clear the movable objects data.
        moveFrom.clear();

        swap( temp );        
    }
    /****************************************************************************************/

    /**
     * De-allocates the objects pointed to by the pointer.
     * If a smart pointer is implemented, it can invoke the destructor of that one.
     */
    ~GenericContainer()
    {
        capacity = 0;
        count = 0;
        delete[] data;
    }


    /****************************************************************************************/
    /*                                   Operator overloads.                                */
    /****************************************************************************************/
    /**
     * Copy assigment overload.
     * Copies everything from target to here.
     * 
     * @param[in] copyFrom                  The object to copy data from.
     * @return                              A reference to the object that invoked this.
     */             
    template< typename U >               
    GenericContainer< T >& operator = ( const GenericContainer< U >& copyFrom )
    {
        if( std::is_convertible< T, U >::value )
        {
            GenericContainer< T > temp( copyFrom.getCapacity() );

            try
            {
                std::copy( copyFrom.cbegin(), copyFrom.cend(), temp.begin() );
                temp.count = copyFrom.getCount();
                swap( temp );
            }
            catch( ... )
            {
                throw;
            }
        }
        return *this;
    }
    /****************************************************************************************/

    /**
     * Copy assigment overload.
     * Copies everything from target to here.
     * 
     * @param[in] copyFrom                  The object to copy data from.
     * @return                              A reference to the object that invoked this.
     */   
    GenericContainer< T >& operator = ( const GenericContainer< T >& copyFrom )
    {
        GenericContainer< T > temp( copyFrom.getCapacity() );

        std::copy( copyFrom.cbegin(), copyFrom.cend(), temp.begin() );

        temp.count = copyFrom.count;

        swap( temp );

        return *this;
    }
    /****************************************************************************************/

    /**
     * Move assignment overload.
     * Moves everything from target to here.
     * 
     * @param[in] moveFrom                  The object to move data from.
     * @return                              A reference to the object that invokes the move assignment.
     */
    template< typename U >
    GenericContainer< T >& operator = ( GenericContainer< U >&& moveFrom )
    {
        GenericContainer< U > temp( static_cast< GenericContainer< U&& > >( moveFrom ) );

        swap( temp );

        return *this;
    }
    /****************************************************************************************/

    /**
     * Dereference operator overload.
     * 
     * @return                              A reference to the object dereferenced to.
     */
    T& operator * ()
    {
        return *data;
    }
    /****************************************************************************************/

    /**
     * Index operator overload.
     * 
     * @param[in] index                     Index number requested.
     * @return                              Reference to the object requested.
     */
    T& operator [] ( int index )
    {
        if( index < count )
        {
            return data[ index ];
        }
        else
        {
            return data[ 0 ];
        }
        
    }
    /****************************************************************************************/


    /****************************************************************************************/
    /*                                   Class methods.                                     */
    /****************************************************************************************/
    /**
     * Gets object in front.
     * 
     */
    T& front( void )
    {
        return *data;
    }
    /****************************************************************************************/
    
    T& back( void )
    {
        return data[ count - 1 ];
    }
    /****************************************************************************************/
    
    /**
     * Clears information.
     * 
     */
    void clear( void )
    {
        data = nullptr;
        capacity = 0;
        count = 0;
    }
    /****************************************************************************************/

    /**
     * Checks wether the container is empty or not.
     * 
     * @return                              Boolean, empty or not.
     */
    bool empty( void ) const
    {
        if ( count != 0 )
        {
            return true;
        }
        return false;
    }
    /****************************************************************************************/

    /**
     * Returns the count in the container.
     * 
     * @return                              The number of objects in the container.
     */
    int size( void ) const
    {
        return count;
    }
    /****************************************************************************************/

    /**
     * Retrieves the theoritical max size of the container.
     * 
     * @return                              The theoretic max of the container.
     */
    auto max_size( void )
    {
        return std::numeric_limits< T >::max();
    }
    /****************************************************************************************/

    /**
     * Works on a container at an index value.
     * 
     * @param[in] index                     The index that is to be accessed.
     * @return                              A reference to the object at the index.
     */
    T& at( int index )
    {
        return data[ index ];
    }
    /****************************************************************************************/

    /**
     * Pushes an object onto the back.
     * 
     */
    void pushBack( const T& toPush )
    {
        if ( capacity > 0 )
        {
            if( count == capacity )
            {
                GenericContainer< T > temp( ( capacity * 2 ) );
                std::copy( begin(), end(), temp.begin() );
                temp.count = count + 1;
                temp[ ( temp.count - 1 ) ] = toPush;
                swap( temp );
            }
            else
            {
                GenericContainer< T > temp( capacity );
                std::copy( begin(), end(), temp.begin() );
                temp.count = count + 1;
                temp[ ( temp.count - 1 ) ] = toPush;
                swap( temp );
            }
        }
        else
        {
            GenericContainer< T > temp( ( 10 ) );
            temp.count = 1;
            temp[ ( temp.count - 1 ) ] = toPush;
            swap( temp );
        }
        
        
        
        
    }
    /****************************************************************************************/
    
    /**
     * Pops an object in the back.
     * 
     */
    void popBack( void )
    {
        if( count > 0 )
        {
            GenericContainer< T > temp( this->capacity );

            temp.count = count;

            //algorithms::copyWithIterator( begin(), end() - 1, temp.begin() );

            temp.count--;

            swap( temp );
        }
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    bool reserve( int slots )
    {
        if( slots > capacity )
        {
            GenericContainer< T > temp( slots );

            std::copy( begin(), end(), temp.begin() );

            temp.count = count;

            swap( temp );

            return true;
        }
        return false;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    int getCount( void ) const
    {
        return count;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    int getCapacity( void ) const
    {
        return capacity;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    bool shrink_to_fit( void )
    {
        GenericContainer< T > temp( count );

        swap( temp );
        // After swap, data is null, and count is null.
        count = temp.count;

        std::copy( temp.begin(), ( temp.begin() + count ), begin() );
        

        return true;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    void swap( GenericContainer< T >& swapFrom )
    {
        // Stores old data.
        T* tempPtr = data;
        int tempCapacity = capacity;
        int tempCount = count;

        // Saves swappable data.
        data = swapFrom.data ;
        capacity = swapFrom.capacity;
        count = swapFrom.count;

        // Swaps to the other one aswell.
        swapFrom.data = tempPtr;
        swapFrom.capacity = tempCapacity;
        swapFrom.count = tempCount;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    iterator begin( void )
    {
        return data;
    }
    /****************************************************************************************/
    
    /**
     * @brief 
     * 
     * @return iterator 
     */
    iterator end( void )
    {
        return &( data[ count ] );
    }
    /****************************************************************************************/

    /**
     * @brief 
     * 
     * @return iterator 
     */
    iterator endCap( void )
    {
        return &( data[ capacity ] );
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    const_iterator cbegin( void ) const
    {
        return data;
    }
    /****************************************************************************************/
    
    /**
     * 
     */
    const_iterator cend( void ) const
    {
        return &( data[ capacity ] );
    }
    /****************************************************************************************/

    void printInfoForDebug( void ) const
    {
        std::cout << "\nMy capacity is: " << capacity << " and my count is: " << count << " ";
        if ( data != nullptr )
        {
            std::cout << "my data is different from nullptr!\n";
            std::cout << "And data is: ";
            for ( int i = 0; i < count; i++ )
            {
                std::cout << data[ i ] << " ";
            }
            
        }
        else
        {
            std::cout << "my data is equal nullptr...\n";
        }
        std::cout << "\n";
    }
};
/****************************************************************************************/


template< typename T, typename U >
bool operator == ( const GenericContainer< T >& left, const GenericContainer< U >& right )
{
    if ( std::is_convertible< T, U >::value )
    {
        if( left.size() == right.size() )
        {
            for ( int i = 0; i < left.size(); i++)
            {
                if ( left.at( i ) != right.at( i ) )
                {
                    return false;
                }
            }
            return true;
        }
    }
    return false;
}
/****************************************************************************************/

template< typename T, typename U >
bool operator != ( const GenericContainer< T >& left, const GenericContainer< U >& right )
{
    if ( left == right )
    {
        return false;
    }
    else
    {
        return true;
    }
}
/****************************************************************************************/



#endif //__GENERIC_CONTAINER_HPP__