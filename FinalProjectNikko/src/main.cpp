#include "../include/GenericContainerVoidPtrTest.hpp"
#include "../include/GenericContainerTest.hpp"
#include <iostream>



int main( int argc, char** args )
{
    testing::InitGoogleTest( &argc, args );
    return RUN_ALL_TESTS();
}