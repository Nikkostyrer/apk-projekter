/*****************************************/
/* Copyright: DevelEdu 2013              */
/* Author: sha                           */
/*****************************************/




#include <fstream>
#include <iostream>
#include <iterator>

#include <algorithm>
#include <numeric>
#include <functional>
#include <vector>
#include <string>
#include <assert.h>
#include "MyArray.h"


template<class T, class Compare> 
constexpr const T& clamp( const T& v, const T& lo, const T& hi, Compare comp )
{ 
	return assert( !comp(hi, lo) ), comp(v, lo) ? lo : comp(hi, v) ? hi : v;
}
template<class T>
constexpr const T& clamp( const T& v, const T& lo, const T& hi ) 
{ 
	return ::clamp( v, lo, hi, std::less<>() ); 
}











#define PRODUCT_DB_FILE		"product.db"

class Product
{
public:
  Product(const std::string& name, float price, unsigned int sold = 0)
    : name_(name), price_(price), sold_(sold)
  {}
  Product()
    : name_(""), price_(0), sold_(0)
  {}
  
  const std::string& name() const
  {
    return name_;
  }
  
  float price() const
  {
    return price_;
  }

  void setPrice(float newPrice)
  {
    price_ = newPrice;
  }
  
  unsigned int sold() const
  {
    return sold_;
  }
  
  friend std::istream& operator>> ( std::istream& i, Product& p );
  friend std::ostream& operator<< ( std::ostream& o, const Product& p );
  
private:
  std::string   name_;
  float         price_;
  int           sold_;
};
typedef std::vector<Product>  ProductList;


std::ostream& operator<< ( std::ostream& o, const Product& p )
{ 
  return o << p.name_ << " " << p.price_ << " " << p.sold_; 
}

std::istream& operator>> ( std::istream& i, Product& p )
{
    return i >> p.name_ >> p.price_ >> p.sold_;
}



/**
 * Read products from file
 */
void productDBRead(ProductList& pl, const std::string& fileName)
{
  pl.clear();
  std::ifstream pFile( fileName.c_str() );
  while( !pFile.eof() )
  {
    Product p;
    pFile >> p;
    if( pFile ) pl.push_back( p );
  }
}


/**
 * Read products from file

 O W N   A D D I T I O N 
 
 */
void prodDBRead(ProductList& pl, const std::string& fileName)
{
	pl.clear();
  // Creating a stream to read from file.
	std::ifstream pFile(fileName.c_str());
	
  // Create istream_iterator to read from the file stream.
	std::istream_iterator<Product> it1(pFile);		// Start of file
	std::istream_iterator<Product> it2;				    // End of file (Default constructor sets iterator at end of file)

  // Call back_inserter with every product from the instream.
	copy(it1, it2, std::back_inserter(pl));			// 
}

/**
 * Printout all products
 */
void printAll(const ProductList& pl)
{
  std::cout << "##################################################" << std::endl;
  std::cout << "Printing out all products..." << std::endl;
  std::cout << "----------------------------" << std::endl;
  for(ProductList::const_iterator iter = pl.begin(); iter != pl.end(); ++iter)
  {
    std::cout << *iter << std::endl;

  }  
  std::cout << "##################################################" << std::endl;
}

/**
 * Printout all products
 */
void printEverything(const ProductList& pl)
{
	std::cout << "##################################################" << std::endl;
	std::cout << "Printing out all products..." << std::endl;
	std::cout << "----------------------------" << std::endl;
	
  // Creating an ostream_iterator that looks at the std::cout
  // This also adds a newline after each text-output.
	std::ostream_iterator<Product> it1(std::cout, "\n");

  // Copies all Product-strings to it1 that then throws it into the cout-tummy
	std::copy(pl.begin(), pl.end(), it1);		
  // Prints cout-tummy.		.
	std::cout;											

	std::cout << "##################################################" << std::endl;
}



/**
   Add item
*/
void addItem(ProductList& pl)
{
  // Getting productName
	std::cout << "Write Product name. Finish with Enter: " << std::endl;
	std::string productName;
	std::cin >> productName;

  // Getting productPrice
	std::cout << "Write Product price. Finish with Enter: " << std::endl;
	float productPrice;
	std::cin >> productPrice;

  // Getting amount of products sold.
	std::cout << "Write amount of products sold. Finish with Enter: " << std::endl;
	int productSold;
	std::cin >> productSold;

  // Pushes the new product into the product list.
	pl.push_back(Product(productName, productPrice, productSold));
}


/**
   Write data to db file
*/
void productDBWrite(const ProductList& pl, const std::string& fileName)
{
  // Creates a writing stream into the file.
	std::ofstream pFile(fileName.c_str());
  // Creates and iterator that jump "products."
	std::ostream_iterator<Product> it1(pFile);
  // Copies from the productlist to the outputstream.
	std::copy(pl.begin(), pl.end(), it1);
}


/**
 * Print poorly selling products
 */
void printPoorlySellingProducts(const ProductList& pl)
{
	std::ostream_iterator<Product> it(std::cout, "\n");

	// Removes copy_if (true).
	// So if the product sold-count is above 10, it'll be removed.
	std::remove_copy_if(pl.begin(), pl.end(), it,
		// This lampda is given the variable that p1 is looking at at this very moment. It is a Product in the list.
		[](Product prd)								
		{
			return (prd.sold() > 10);				// returns false if sold() is less than 10.
		}
	);
}


/**
 * Set a discount on all products - Using for_each()
 */
void addDiscountUsingForEachLambda(ProductList& pl)
{
	std::for_each( pl.begin(), pl.end(), []( Product& p )
	{
		p.setPrice( p.price() * 0.9 );
	});
}


class Helper
{
	public:
	void operator()( Product& p )
	{
		p.setPrice( p.price() * 0.9 );
	}
};
/**
 * Set a discount on all products - Using for_each()
 */
void addDiscountUsingForEachFunctor(ProductList& pl)
{
	Helper myHelper;
	std::for_each( pl.begin(), pl.end(), myHelper );
}

Product& doSome( Product p )
{}

/**
 * Set a discount on all products - Using transform()
 */
void addDiscountUsingTransform(ProductList& pl)
{
	std::transform( pl.begin(), pl.end(), pl.begin(), []( Product& p )->Product&
	{
		// Get some discount
		std::cout << "Write discount for product: " << p << "\n";
		double disc = 0;
		std::cin >> disc;

		disc = clamp( static_cast<int>( disc ), 10, 90 );

		// return [](  )->Product& 
		// {
		// 	return Product("myprod", 2, 2 );
		// };


		return p;	
	}
	);
}


/**
 * Calculate the total amount of sold products
 */
void calcTotalSoldProducts(ProductList& pl)
{
	int sum = 0;

	// std::accumulate calls the binaryOperation function with both sum and p.
	sum = std::accumulate( pl.begin(), pl.end(), sum, []( int sumz, Product& p )
	{
		return p.sold() + sumz;
	});

	std::cout << "Total number of items sold is: " << sum << "\n";
}


/**
 * Setting discount using bind2nd - OPTIONAL
 */


int main()
{
  bool        running = true;
  ProductList pl;
  
//   while(running)
//   {
//     char choice;
    
//     std::cout << "********************" << std::endl;
//     std::cout << "Help menu: " << std::endl;
//     std::cout << "'1' Read product database" << std::endl;
//     std::cout << "'2' Print all items" << std::endl;
//     std::cout << "'3' Add item" << std::endl;
//     std::cout << "'4' Write product database" << std::endl;
//     std::cout << "'5' Print poorly selling products" << std::endl;
//     std::cout << "'6' Set a discount on all products (using for_each() with lambda )\n";
//     std::cout << "'7' Set a discount on all products (using transform() )" << std::endl;
//     std::cout << "'8' Calculate the total amount of sold products" << std::endl;
// 	std::cout << "'9' Set a discount on all products ( using for_each with functor ) \n";
    
    
//     std::cout << "'q' Quit" << std::endl;
//     std::cout << "Your choice: ";
//     std::cin >> choice;
    
//     switch(choice)
//     {
//       	case '1':
//         	prodDBRead(pl, PRODUCT_DB_FILE);
//         	break;

//       	case '2':
// 		  	printEverything(pl);
//         	break;

//       	case '3':
//         	addItem(pl);
//         	break;

//       	case '4':
//         	productDBWrite(pl, PRODUCT_DB_FILE);
//         	break;

//       	case '5':
//         	printPoorlySellingProducts(pl);
//         	break;
        
//       	case '6':
//         	addDiscountUsingForEachLambda(pl);
//         	break;

//       	case '7':
//         	addDiscountUsingTransform(pl);
//         	break;

//       	case '8':
//         	calcTotalSoldProducts(pl);
//         	break;

// 		case '9':
//         	addDiscountUsingForEachLambda(pl);
//         	break;
        
//       	case 'q':
//       	case 'Q':
//         	running = false;
//     }
    
    
//   }





 	// Testin of MyArray class and own iterator.
 	MyArray< int, 5 > firstArray;
 	MyArray< int, 5 > secondArray;

	// Fills first array
 	firstArray.fill( 19 );

	// Prints both arrays.
 	firstArray.print();
 	secondArray.print();

	// Sets an iterator on the start of firstArray
 	MyIterator< int > beginIt( firstArray.begin() );
	// Sets an iterator on end of firstArray
 	MyIterator< int > endIt( firstArray.end() );
	// Sets and iterator on start of secondArray
 	MyIterator< int > toInsert( secondArray.begin() );

	// Copies from firstArray to secondArray
 	std::copy( beginIt, endIt, toInsert );

 	firstArray.print();
 	secondArray.print();

 	std::ostream_iterator< int > it1(std::cout, "\n");

//   // Copies all Product-strings to it1 that then throws it into the cout-tummy
	std::copy(beginIt, endIt, it1);		
   // Prints cout-tummy.		.
 	std::cout;		
}