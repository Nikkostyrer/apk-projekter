#ifndef __MY_ARRAY_HPP__
#define __MY_ARRAY_HPP__
#include <type_traits>
#include <iostream>
#include <iterator>

template< typename T >
class MyIterator : public std::iterator<std::input_iterator_tag, T >
{
	public:
	MyIterator( T* x )
	{
		p = x;
	}

	MyIterator( const MyIterator& mit )
	{
		p = mit.p;
	}

	MyIterator& operator++()
	{
		p++;
		return *this;
	}

	MyIterator operator++( int )
	{
		MyIterator tmp( *this );
		operator++();
		return tmp;
	}


	bool operator==( const MyIterator& rhs ) const
	{
		return p == rhs.p;
	}

	bool operator!=( const MyIterator& rhs ) const
	{
		return p != rhs.p;
	}

	T& operator*()
	{
		return *p;
	}


	private:
	T* p;
};

template< typename T, typename V >
T* myFind( T* first, T* last, const V v )
{
	static_cast< T >( v );
	for ( auto i = first; i < last; i++ )
	{
		if ( v == *i )
		{
			return i;
		}
	}
	return last;
}

template <typename T, typename V>
T** myFind( T** first, T** last, const V& v )
{
	for ( auto i = first; i < last; i++ )
	{
		if ( v == **i)
		{
			return i;
		}
	}
	return last;
}


/**
 * MyArray template class definition.
 * 
 * Acts like normal array, but with extra functionality.
 *
 */
template< typename T, int arraySize >
class MyArray
{
public:
	/************************************************************
	 *						Constructors.		    			*
	 ************************************************************/
	MyArray()
	{
	}

	/************************************************************/
	~MyArray()
	{

	}


	friend std::ostream& operator<< ( std::ostream& o, const MyArray< T, arraySize>& MA );

	/************************************************************/
	template< typename Z, int R >
	MyArray( MyArray< Z, R >& copyFrom )
	{
		if ( std::is_convertible< Z, T >::value )
		{
			if ( copyFrom.size() >= sizeOfArray )
			{
				for ( auto i = begin(); i < end(); i++ )
				{
					int counter = 0;
					*i = static_cast< T >( copyFrom[ counter++ ] );
				}
			}
			else
			{
				for ( int i = 0; i < copyFrom.size(); i++ )
				{
					actualArray[ i ] = static_cast< T >( copyFrom[ i ] );
				}
			}
		}
		else
		{
			std::cout << "Error: The two types are not compatible!" << std::endl;
		}
	}

	/************************************************************
	 *						Class methods.						*
	 ************************************************************/
	void fill( const T& toFill )
	{
		for ( int i = 0; i < sizeOfArray; i++ )
		{
			actualArray[ i ] = toFill;
		}
	}

	/************************************************************/
	T* const begin( void )
	{
		return actualArray;
	}


	/************************************************************/
	T* const end( void )
	{
		return &( actualArray[ sizeOfArray ] );
	}

	/************************************************************/
	size_t size( void ) const
	{
		return sizeOfArray;
	}

	void print( void )
	{
		for (size_t i = 0; i < arraySize; i++)
		{
			std::cout << actualArray[i] << "\n";
		}
		
	}
	

	/************************************************************
	 *						Operator overloads.					*
	 ************************************************************/
	T& operator[]( int i )
	{
		return actualArray[ i ];
	}

	/************************************************************/
	template< typename Z, int R >
	MyArray< T, arraySize >& operator=( MyArray< Z, R >& copyFrom )
	{
		//if ( std::is_convertible< Z, T >::value )
		//{
			if ( copyFrom.size() >= sizeOfArray )
			{
				for ( auto i = begin(); i < end(); i++ )
				{
					int counter = 0;
					*i = static_cast< T >( copyFrom[ counter++ ] );
				}
			}
			else
			{
				for ( int i = 0; i < copyFrom.size(); i++ )
				{
					actualArray[ i ] = static_cast< T >( copyFrom[ i ] );
				}
			}
		//}
		//else
		//{
			//std::cout << "Error: The two types are not compatible!" << std::endl;
		//}
		return *this;
	}
	
	/************************************************************
	 *						  Class members.					*
	 ************************************************************/
private:
	T actualArray[ arraySize ] = { 0 };
	int sizeOfArray = arraySize;
};

template< typename T, int arraySize >
std::ostream& operator<< ( std::ostream& o, const MyArray< T, arraySize>& MA )
{ 
	for (size_t i = 0; i < arraySize; i++)
	{
		o << MA.actualArray[ i ] << "\n";
	}
  	return o; 
}






template< typename T, int arraySize >
class MyArray< T*, arraySize >
{
public:
	~MyArray()
	{
		for ( int i = 0; i < sizeOfArray; i++ )
		{
			delete actualArray[ i ];
		}
	}

	T** begin( void )
	{
		return actualArray;
	}

	T** end( void )
	{
		return &( actualArray[ sizeOfArray ] );
	}

	T*& operator[]( int i )
	{
		return actualArray[ i ];
	}

	void fill( T* toFill )
	{
		for ( int i = 0; i < sizeOfArray; i++ )
		{
			actualArray[ i ] = new T( *toFill );
		}
	}

  private:
	  T* actualArray[ arraySize ] = { nullptr };
	  int sizeOfArray = arraySize;
};



#endif // !__MY_ARRAY_HPP__