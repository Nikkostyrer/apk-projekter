#include "../include/File.hpp"
#include "../include/copyAble.hpp"
#include "../../googletest/googletest/src/gtest-all.cc"

// Testing writing to file.
TEST( WritingToFile, simpleWrite )
{
    File firstText( ( char* )"FirstText.txt", ( char* )"wt" );
    firstText.write( ( char* )"This is the result of the first test." );
}

// Testing reading from file.
TEST( ReadingFromFile, simpleRead )
{
    File firstText( ( char* )"FirstText.txt", ( char* )"rt" );
    firstText.read1();
}

// Testing copy constructor
TEST( COPYING, copyconstructor )
{
    File toBeCopied( ( char* )"CopyText.txt", ( char* )"wt" );
    File imACopy( toBeCopied );

    imACopy.write( (char* )"This is being printed in copy assignment, but it's the result of copy constructor." );
}

// Testing copy assignment
TEST( COPYING, copyassignment )
{
    File toBeCopied( ( char* )"CopyText.txt", ( char* )"rt" );
    File imACopy = toBeCopied;

    imACopy.read1();
}

// Testing move constructor
TEST( MOVE, constructor )
{
    File toBeMoved( ( char* )"MoveText.txt", ( char* )"wt" );
    File imMover( std::move( toBeMoved ));


    imMover.write( ( char* )"This is printed in move assignment, but it's the result of move constructor." );
}

// Testing move assignment
TEST( MOVE, assignment )
{
    File toBeMoved( ( char* )"MoveText.txt", ( char* )"rt" );
    File imMover( std::move( toBeMoved ));


    imMover.read1( );
}


// Testing a vector with copyable objects
TEST( VECTOR, copyables )
{
    std::vector< CopyAble > vector1;
    CopyAble first( 1, 1 );
    CopyAble second( 2, 2 );
    CopyAble third( 3, 3 );
    CopyAble fourth( 4, 4 );
    CopyAble fifth( 5, 5 );

    std::cout << "This is a print after creating 5 copyables. They are being pushed now!\n";

    vector1.push_back( first );
    std::cout << "After first push!\n";
    vector1.push_back( second );
    std::cout << "After second push!\n";
    vector1.push_back( third );
    std::cout << "After third push!\n";
    vector1.push_back( fourth );
    std::cout << "After fourth push!\n";
    vector1.push_back( fifth );
    std::cout << "After fifth push!\n";

    std::vector< CopyAble > vector2;

    std::cout << "Moving the entire thing now\n";
    std::move( std::begin( vector1 ), std::end( vector1 ), std::back_inserter( vector2 ));

    EXPECT_EQ( (*(vector2.begin())).getFirstVal(), 1 );
    EXPECT_EQ( (*(vector2.begin())).getFirstVal(), 0 );
}


TEST( VECTORAGAIN, moving )
{
    // Testing the vector after implementing moving semantics.
    // Vector metoden emplace_back er en variadic template function, som kan move en masse argumenter på en gang.
    std::vector< CopyAble > vector1;

    std::cout << "Emplacing back now!\n";

    vector1.emplace_back( CopyAble( 1, 1 ), CopyAble( 2, 2 ), CopyAble( 3, 3 ), CopyAble( 4, 4 ), CopyAble( 5, 5 ) );

}





int main( int argc, char** args )
{
    testing::InitGoogleTest( &argc, args );
    return RUN_ALL_TESTS();
}