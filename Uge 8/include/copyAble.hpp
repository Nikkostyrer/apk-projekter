#ifndef __COPYABLE_HPP__
#define __COPYABLE_HPP__

#include <iostream>

class CopyAble
{
public:
    CopyAble( int firstIn, int secondIn )
    {
        firstVal = firstIn;
        secondVal = secondIn;
    }

    CopyAble()
    {
        firstVal = 40;
        secondVal = 80;
    }

    int getFirstVal( void )
    {
        return firstVal;
    }

    CopyAble( const CopyAble& rhs )
    {
        firstVal = rhs.firstVal;
        secondVal = rhs.secondVal;

        std::cout << __PRETTY_FUNCTION__ << "\n";
    }

    CopyAble& operator=( const CopyAble& rhs )
    {
        firstVal = rhs.firstVal;
        secondVal = rhs.secondVal;
        
        std::cout << __PRETTY_FUNCTION__ << "\n";
        return *this;
    }

    CopyAble( CopyAble&& rhs )
    {
        firstVal = rhs.firstVal;
        secondVal = rhs.secondVal;

        rhs.firstVal = 0;
        rhs.secondVal = 0;

        std::cout << __PRETTY_FUNCTION__ << "\n";
    }

    CopyAble& operator=( CopyAble&& rhs )
    {
        firstVal = rhs.firstVal;
        secondVal = rhs.secondVal;

        rhs.firstVal = 0;
        rhs.secondVal = 0;
        
        std::cout << __PRETTY_FUNCTION__ << "\n";
        return *this;
    }

private:
    int firstVal = 0;
    int secondVal = 10;
};












#endif //__COPYABLE_HPP__