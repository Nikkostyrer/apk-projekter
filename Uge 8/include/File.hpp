#ifndef __FILE_HPP__
#define __FILE_HPP__

#include <stdio.h>
#include <iostream>
#include <string.h>
#include <windows.h>


std::ostream& red(std::ostream &s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout, 
                FOREGROUND_RED|FOREGROUND_INTENSITY);
    return s;
}

std::ostream& white(std::ostream &s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    SetConsoleTextAttribute(hStdout, 
       FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
    return s;
}



class File
{
public:
    File( const char* filename, char* mode );

    File( const std::string name, char* mode )
    {
        fileName = name;
        fp = fopen( fileName.c_str(), mode );
    }

    std::string getFileName( void ) const
    {
        return fileName;
    } 

    /**
     * Copy constructor
     */
    File( const File& rhs )
    {
        std::cout << "Copy contructor called\n";
        textSize = rhs.textSize;
        fileText = rhs.fileText;
        fp = rhs.fp;
    }

    /**
     * Move constructor.
     */
    File( File&& rhs)
    {
        std::cout << "Move constructor called\n";
        textSize = rhs.textSize;
        rhs.textSize = 0;

        fileText = rhs.fileText;
        rhs.fileText = nullptr;

        fp = rhs.fp;
        rhs.fp = nullptr;
    }

    /**
     * Copy assignment.
     */
    File& operator=( const File& rhs )
    {
        textSize = rhs.textSize;
        fileText = rhs.fileText;
        fp = rhs.fp;

        return *this;
    }

    /**
     * Move assignment.
     */
    File& operator=( File&& rhs )
    {
        textSize = rhs.textSize;
        rhs.textSize = 0;

        fileText = rhs.fileText;
        rhs.fileText = nullptr;

        fp = rhs.fp;
        rhs.fp = nullptr;

        return *this;
    }

    /**
     * Destructor
     */
    ~File()
    {
        if ( fp != nullptr)
        {
            fclose( fp );
        }
        
        delete fileText;
    }

    void read1( void );
    void write( char* toWrite );
private:
    FILE* fp = nullptr;

    // Use these to allocate a place to read into.
    char* fileText = nullptr;
    int textSize = 0;

    std::string fileName;
};







File::File( const char* filename, char* mode )
{
    fp = fopen( filename, mode );
    if ( fp == nullptr || fp == NULL )
    {
        std::cout << red << "There was an error opening the file!\n" << white;
    }
    
}

void File::read1( void )
{
    // Acquire size of file.
    if( fseek( fp, 0, SEEK_END ) )
    {
        std::cout << red << "There was an error seeking the file\n";
    }
    long fileSize = ftell( fp );
    if ( fileSize == -1 )
    {
        std::cout << red << "Error in retrieving filesize\n";
    }
    
    

    fseek( fp, 0, SEEK_SET );

    // Allocate a place to save the text to.
    textSize = fileSize;
    fileText = new char[ fileSize ];

    // Read from file into buffer.
    int result = fread( fileText, 1, fileSize, fp );
    if ( result != fileSize )
    {
        std::cout << "Read only: " << result / sizeof( fileText[ 1 ] ) << " chars, but needed: " << fileSize << "\n";
    }


    std::cout << fileText << "\n";
}


void File::write( char* toWrite )
{
    int result = fwrite( toWrite, 1, strlen( toWrite ), fp );
    if ( result != strlen( toWrite ) )
    {
        std::cout << red << "There was an error writing to the file\n";
    }
    
}

#endif //__FILE_HPP__