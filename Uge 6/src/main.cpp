#include "../include/MyVector.hpp"
#include "../../googletest/googletest/src/gtest-all.cc"

TEST( MyVectors, BasicConstructor )
{
    MyVector< int > firstVector( 10 );

    ASSERT_EQ( firstVector[ 3 ], 10 );
    ASSERT_EQ( firstVector[ 7 ], 10 );
}

TEST( MyVectors, CopyConstructor)
{
    MyVector< int > firstVector( 10 );
    MyVector< int > secondVector( firstVector );

    ASSERT_EQ( firstVector, secondVector );
}

TEST( MyVectors, AssignmentOp )
{
    MyVector< int > firstVector( 10 );
    MyVector< int > secondVector = firstVector;

    ASSERT_EQ( firstVector, secondVector );
}

TEST( MyVectors, pushBack )
{
    MyVector< double > firstVector( 20 );
    firstVector.push_back( 20.1 );
    ASSERT_EQ( firstVector[20], 20.1 );
}

TEST( MyVectors, popBack )
{
    MyVector< double > firstVector( 20 );
    firstVector.pop_back();
    ASSERT_EQ( firstVector.size(), 19 );
}

TEST( MyVectors, insert )
{
    MyVector< double > firstVector( 20 );
    firstVector.insert( 40.0, 2 );
    ASSERT_EQ( firstVector[ 2 ], 40.0 );
}


int main( int argc, char* argv[] )
{
    testing::InitGoogleTest( &argc, argv );

    return RUN_ALL_TESTS();
}