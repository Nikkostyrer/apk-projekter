#include <chrono>
#include <future>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <iterator>
#include <algorithm>
#include <thread>

class Algo
{
public:
  Algo()
  {

  }

  Algo(std::vector<int> v)
    : data_(std::move(v))
  {}
    
  void doAlgo()
  {
    std::chrono::time_point<std::chrono::high_resolution_clock> commence = std::chrono::high_resolution_clock::now();
    std::sort(data_.begin(), data_.end());
    std::chrono::time_point<std::chrono::high_resolution_clock> complete = std::chrono::high_resolution_clock::now();
    duration_ = std::chrono::nanoseconds(complete - commence);
  }
    
  std::chrono::duration<long double> processingTime()
  {
    return duration_;
  }
    
    
  void print()
  {
    std::copy(data_.begin(), data_.end(), std::ostream_iterator<int>(std::cout, " "));
  }
private:
  Algo(const Algo&) = delete; // No copy construction is allowed
    
  Algo& operator=(const Algo&) = delete; // No copy assignment is allowed

  std::vector<int>                    data_;
  std::chrono::nanoseconds            duration_;
};


typedef Algo SmartAlgo;
typedef Algo CoolAlgo;
typedef Algo FantasticAlgo;

int main( int argc, char* argv[] )
{
  int i = 0;
  int n = 10000000; // Change at your leasure
    
  std::vector<int> data;
  data.reserve(n);
  generate_n( back_inserter( data ), n, [&i](){return i++;}  );
  std::random_shuffle( data.begin(), data.end() );

  // Create three shared pointers to share algorithm data.
  std::shared_ptr< Algo > mainPtr1 = std::make_shared< Algo >();
  std::shared_ptr< Algo > mainPtr2 = std::make_shared< Algo >();
  std::shared_ptr< Algo > mainPtr3 = std::make_shared< Algo >();


  // After creation of identical data, 3 threads are to be created.
  std::thread firstThread( [ &mainPtr1 ]( std::vector< int > vectorIn ){
    std::shared_ptr< SmartAlgo > firstAlgo( new SmartAlgo( vectorIn ));
    mainPtr1 = firstAlgo;

    // Call doAlgo
    (*firstAlgo).doAlgo();
  }, data );


  std::thread secondThread( [ &mainPtr2 ]( std::vector< int > vectorIn ){
    std::shared_ptr< CoolAlgo > secondAlgo( new CoolAlgo( vectorIn ));
    mainPtr2 = secondAlgo;

    // Call doAlgo
    (*secondAlgo).doAlgo();
  }, data );


  std::thread thirdThread( [ &mainPtr3 ]( std::vector< int > vectorIn ){
    std::shared_ptr< FantasticAlgo > thirdAlgo( new FantasticAlgo( vectorIn ));
    mainPtr3 = thirdAlgo;

    // Call doAlgo
    (*thirdAlgo).doAlgo();
  }, data );



  // Wait for the threads to finish up before terminating the program
  firstThread.join();
  secondThread.join();
  thirdThread.join();


  auto timeOfThread1 = (*mainPtr1).processingTime();
  auto timeOfThread2 = (*mainPtr2).processingTime();
  auto timeOfThread3 = (*mainPtr3).processingTime();

  std::cout << "Time of first thread: " << timeOfThread1.count() << "\nTime of second thread: " << timeOfThread2.count() << "\nTime of third thread: " << timeOfThread3.count() << "\n";
  
  return 0;
}
