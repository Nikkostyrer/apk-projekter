#include "../../googletest/googletest/src/gtest-all.cc"
#include "../include/adderFunc.hpp"
#include "../include/yaPrint.hpp"
#include "../include/AttractiveForce.hpp"
#include <iostream>



TEST( VariadicTemplate, adder )
{
    int sum = adder( 1, 2, 4, 3, 5, 6, 8, 19 );

    EXPECT_EQ( sum, 1+2+4+3+5+6+8+19 );
}

TEST( yaprint, TrueTest )
{
    ya_print("Some string that is % and % and %", 10, 2, 4 );
    std::cout << "\n";
}

TEST( attractiveforce, BasicTest )
{
    const Mass kilogram( 1 );
    const Distance meter( 1 );
    const Time second( 1 );

    ASSERT_EQ( kilogram.value(), meter.value() );
    ASSERT_EQ( kilogram.value(), second.value() );

    Mass myWeight( 134.0 * kilogram );

    ASSERT_EQ( myWeight.value(), 134.0 );
}

TEST( attractiveforce, ForceCalculation )
{
    const GravitationalUnit GravitationalConstant( 6.674 * ( pow( 10, -11 )));
    const Mass kilogram( 1 );
    const Distance meter( 1 );
    const Time second( 1 );

    Mass Moon( 7.35* ( pow( 10, 22 )));
    Mass Earth( 5.972 * ( pow( 10, 24 )));

    Distance EarthAndMoon( 384400 * 1000 );

    EXPECT_EQ( Moon.value(), 7.35 * pow( 10, 22 ) );
    EXPECT_EQ( Earth.value(), 5.972 * pow( 10, 24 ) );
    EXPECT_EQ( EarthAndMoon.value(), 384400000.0 );

    // The userdefined ForceUnit is of the same unit as the calculated unit. Otherwise it would not compile.
    ForceUnit ForceEarthAndMoon = ( GravitationalConstant * ( ( Moon * Earth ) / ( EarthAndMoon * EarthAndMoon ) ) );

    std::cout << "The value of the force between the earth and the moon is: " << ForceEarthAndMoon.value() << "\n";

}

TEST( attractiveforce, UserDefinedLiterals )
{
    const GravitationalUnit GravitationalConstant( 6.674 * ( pow( 10, -11 )));
    const Mass kilogram( 1 );
    const Distance meter( 1 );
    const Time second( 1 );

    Mass Moon( 73.5_zettagram );
    Mass Earth( 5972.0_zettagram );

    Distance EarthAndMoon( 384400.0_km );

    // v-- This fails for some reason. Even though they look similar in the test
    EXPECT_EQ( Moon.value(), 7.35 * pow( 10, 22 ) ); 
    EXPECT_EQ( Earth.value(), 5.972 * pow( 10, 24 ) );
    EXPECT_EQ( EarthAndMoon.value(), 384400000.0 );

     // The userdefined ForceUnit is of the same unit as the calculated unit. Otherwise it would not compile.
    ForceUnit ForceEarthAndMoon = ( GravitationalConstant * ( ( Moon * Earth ) / ( EarthAndMoon * EarthAndMoon ) ) );

    std::cout << "The value of the force between the earth and the moon is: " << ForceEarthAndMoon.value() << "\n";
}

int main( int argc, char** args )
{
    testing::InitGoogleTest( &argc, args );
    return RUN_ALL_TESTS();
}