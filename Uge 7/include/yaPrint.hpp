#include <stdlib.h>
#include <iostream>
#include <algorithm>

// Below is the implementation of the template class with only one argument.

// void ya_print( const char* s)
// {
//     while(*s)
//     {
//         if(*( s ) == '%' )
//         {
//             if ( *( s + 1) == '%' )
//             {
//                 ++s;
//             }
//             else
//             {
//                 throw std::runtime_error("invalid format string: missing arguments");
//             }
//         }
//     std::cout << *s++;
//     }
// }


template< typename T, typename... Args >
void ya_print( const char* s, T value, Args... args )
{
    while( *s )
    {
        if( *s == '%' )
        {
            if( *( s + 1 ) == '%')
            {
                ++s;
            }
            else
            {
                std::cout << value;
                if constexpr( sizeof...(args) > 0 )
                {
                    ya_print( s + 1, args... );
                }
                return;
            }
        }
        std::cout << *s++;
    }
    throw std::logic_error( "extra arguments provided to printf" );
    
}