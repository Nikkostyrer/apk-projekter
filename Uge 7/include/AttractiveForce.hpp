#ifndef __ATTRACTIVE_FORCE_HPP__
#define __ATTRACTIVE_FORCE_HPP__


#include <math.h>


template< int mass, int distance, int time >
class Units
{
public:
    // Creating the actual value of the unit.
    explicit Units( double initVal = 0 )
    {
        val = initVal;
    }

    // Returning the value, but can't be changed.
    double value( void ) const
    {
        return val;
    }

    // Returns a reference to the value, meaning that it CAN be changed.
    double& value( void )
    {
        return val;
    }

    // Destructor does nothing. Removable.
    ~Units()
    {

    }
    
    // Only doable with equal types.
    Units< mass, distance, time >& operator+=( const Units< mass, distance, time >& rhs )
    {
        val += rhs.val;
        return *this;
    }
    // Only doable with equal types.
    Units< mass, distance, time >& operator-=( const Units< mass, distance, time >& rhs )
    {
        val -= rhs.val;
        return *this;
    }

    Units< mass, distance, time >& operator/=( double rhs )
    {
        val /= rhs;
        return *this;
    }

    Units< mass, distance, time >& operator*=( double rhs )
    {
        val *= rhs;
        return *this;
    }

private:
    double val;
};

typedef Units< 1, 0, 0 > Mass;
typedef Units< 0, 1, 0 > Distance;
typedef Units< 0, 0, 1 > Time;

// Defining a G-unit with distance^2 / ( mass * time^2 )
typedef Units< -1, 3, -2 > GravitationalUnit;
typedef Units< 1, 1, -2 > ForceUnit;


/**
 * These allow for the adding and subtracting of two of the same units.
 * 
 */
template< int mass, int distance, int time >
const Units< mass, distance, time> operator+( const Units< mass, distance, time> lhs, 
                                              const Units< mass, distance, time> rhs )
{
    Units< mass, distance, time> result( lhs );
    return result += rhs;
}
/*********************************************************************************************************/

template< int mass, int distance, int time >
const Units< mass, distance, time> operator-( const Units< mass, distance, time>& lhs, 
                                              const Units< mass, distance, time>& rhs )
{
    Units< mass, distance, time> result( lhs );
    return result -= rhs;
}
/*********************************************************************************************************/



/**
 * These allow for the multiplication of a unit by a number.
 * 
 */
template< int mass, int distance, int time >
const Units< mass, distance, time> operator*( double lhs, 
                                              const Units< mass, distance, time>& rhs )
{
    Units< mass, distance, time> result( rhs );
    return result *= lhs;
}
/*********************************************************************************************************/

template< int mass, int distance, int time >
const Units< mass, distance, time> operator*( const Units< mass, distance, time>& lhs,
                                              double rhs )
{
    Units< mass, distance, time> result( lhs );
    return result *= rhs;
}
/*********************************************************************************************************/



/**
 * These allow for the division of a unit by a number.
 * 
 */
template< int mass, int distance, int time >
const Units< mass, distance, time> operator/( double lhs, 
                                              const Units< mass, distance, time>& rhs )
{
    Units< mass, distance, time> result( rhs );
    return result /= lhs;
}
/*********************************************************************************************************/
template< int mass, int distance, int time >
const Units< mass, distance, time> operator/( const Units< mass, distance, time>& lhs,
                                              double rhs )
{
    Units< mass, distance, time> result( lhs );
    return result /= rhs;
}
/*********************************************************************************************************/



/**
 * The actual handling of division and multiplication of values happens below here.
 * Multiplying two units should provide and typedef a new unit.
 * 
 */
template< int mass1, int distance1, int time1,
          int mass2, int distance2, int time2 >
const Units< mass1 + mass2, distance1 + distance2, time1 + time2 > 
operator*( const Units< mass1, distance1, time1 >& lhs, 
           const Units< mass2, distance2, time2 >& rhs )
{
    // Typedefs the new unit type. Example mass * mass would be defined as Units< 2, 0, 0 >
    typedef Units< mass1 + mass2, distance1 + distance2, time1 + time2 > ResultType;

    return ResultType( lhs.value() * rhs.value() );
}
/*********************************************************************************************************/


template< int mass1, int distance1, int time1,
          int mass2, int distance2, int time2 >
const Units< mass1 - mass2, distance1 - distance2, time1 - time2 > 
operator/( const Units< mass1, distance1, time1 >& lhs, 
           const Units< mass2, distance2, time2 >& rhs )
{
    // Typedefs the new unit type. Example mass^2 / time would be defined as Units< 2, 0, -1 >
    typedef Units< mass1 - mass2, distance1 - distance2, time1 - time2 > ResultType;

    return ResultType( lhs.value() / rhs.value() );
}
/*********************************************************************************************************/



/**
 * User defined literals.
 * 
 */
long double operator"" _tonne( long double kg )
{
    return kg * 1000;
}

long double operator"" _km( long double meter )
{
    return meter * 1000;
}

long double operator"" _zettagram( long double zettagram ) // Sextillion ( 10^21 )
{
    return zettagram * pow( 10, 21 );
}

#endif //__ATTRACTIVE_FORCE_HPP__